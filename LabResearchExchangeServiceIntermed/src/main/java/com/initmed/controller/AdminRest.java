/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import ca.uhn.fhir.model.dstu2.resource.Patient;
import com.initmed.db.model.EgiszOrderitems;
import com.initmed.db.model.EgiszOrders;
import com.initmed.db.model.EgiszResearchtypes;
import com.initmed.db.model.EgiszTargets;
import com.initmed.db.model.Persons;
import com.initmed.db.model.VExlabVLabTestResult;
import com.initmed.db.model.VexlabOutocomeOrders;
import com.initmed.exceptions.WrongDateIntervalException;
import com.initmed.exceptions.WrongDiagnException;
import com.initmed.exceptions.WrongNameException;
import com.initmed.facades.EgiszOrdersFacade;
import com.initmed.facades.EgiszTargetsFacade;
import com.initmed.facades.EventlogFacade;
import com.initmed.facades.PersonsFacade;
import com.initmed.facades.VExlabVLabTestResultFacade;
import com.initmed.facades.VexlabOutocomeOrdersFacade;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sd199
 */
@Stateless
@Path("/admin")
public class AdminRest {

    @EJB
    private EgiszOrdersFacade egiszOrdersFacade;
    @EJB
    private FHIRCLient fHIRCLient;
    @EJB
    private VExlabVLabTestResultFacade exlabVLabTestResultFacade;
    @EJB
    private BundleIncomeLoader bundleIncomeLoader;
    @EJB
    private FHIRparameters fHIRparameters;
    @EJB
    private VexlabOutocomeOrdersFacade vexlabOutocomeOrdersFacade;
    @EJB
    private EventlogFacade eventlogFacade;
    @EJB
    private BundleOutcomeLoader bundleOutcomeLoader;
    @EJB
    private PersonsFacade peopleFacade;
    @EJB
    private EgiszTargetsFacade egiszTargetsFacade;

    @GET
    @Produces("application/json")
    @Path("/rowlist")
    public Response getRowsList() throws JSONException {
        List<EgiszOrders> list = egiszOrdersFacade.getLastBoundles();
        JSONArray values = new JSONArray();
        for (EgiszOrders x : list) {
            JSONObject jo = new JSONObject();
//            if (exlabVLabTestResultFacade.loadResults(x.getEgiszOrderitemsSet().iterator().next().getDocAnalysOrderAim().getId()) != null) {
            if (x.getEgiszOrderitemsSet() != null && !x.getEgiszOrderitemsSet().isEmpty()) {
                EgiszOrderitems еitem = x.getEgiszOrderitemsSet().iterator().next();
                if (еitem != null && еitem.getDocAnalysOrderAim() != null) {
                    List<VExlabVLabTestResult> vexlabTestResult = exlabVLabTestResultFacade.loadResults(еitem.getDocAnalysOrderAim().getId());
                    if (vexlabTestResult != null && !vexlabTestResult.isEmpty()) {
                        Persons ps = vexlabTestResult.get(0).getPersons();
                        if (ps != null) {
                            jo.put("name", ps.getLastName() + " " + ps.getFirstName() + " " + ps.getMiddleName());
                        } else {
                            jo.put("name", "");
                        }
                    } else {
                        jo.put("name", "");
                    }
                } else {
                    jo.put("name", "");
                }
            } else {
                jo.put("name", "");
            }
            jo.put("id", x.getId());
            if (x.getEgiszOrderitemsSet() != null && !x.getEgiszOrderitemsSet().isEmpty()) {
                jo.put("date", x.getEgiszOrderitemsSet().iterator().next().getTakingdate());
            } else {
                jo.put("date", "");
            }
            jo.put("guid", x.getOrederUUID());
            if (x.getEgiszOrderitemsSet() != null && !x.getEgiszOrderitemsSet().isEmpty()) {
                EgiszResearchtypes rt = x.getEgiszOrderitemsSet().iterator().next().getRefegiszResearchtypes();
                if (rt != null) {
                    jo.put("research", rt.getName());
                } else {
                    jo.put("research", "");
                }
            } else {
                jo.put("research", "");
            }
            jo.put("error", x.getErrormsg());
//            }
            values.put(jo);
        }
        JSONObject res = new JSONObject();
        res.put("data", values);
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .entity(res.toString())
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("/order/result/{id}")
    public Response sendResult(@PathParam("id") Integer id) throws JSONException {
        EgiszOrders order = egiszOrdersFacade.find(id);
        String res = "";
        try {
            res = fHIRCLient.sendOrder(order);
        } catch (WrongNameException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .entity(res)
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("/order/status/{id}")
    public Response getStatus(@PathParam("id") Integer id) throws JSONException {
        EgiszOrders order = egiszOrdersFacade.find(id);
        String res = "";
        try {
            if (order.getOrederUUID() != null) {
                res = fHIRCLient.getStatus(order.getOrederUUID());
            } else {
                res = "Результат не был отправлен";
            }
        } catch (WrongNameException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .entity(res)
                .build();
    }
    
        @GET
    @Produces("application/json")
    @Path("/orderresult/{id}")
    public Response orderResult(@PathParam("id") Integer id) throws JSONException {
        EgiszOrders order = egiszOrdersFacade.find(id);
        String res = "";
        try {
            if (order.getOrederUUID() != null) {
                res = fHIRCLient.getStatus(order.getOrederUUID());
            } else {
                res = "Результат не был отправлен";
            }
        } catch (WrongNameException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .entity(res)
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("/syncincomeorders")
    public Response syncIncomeOrders() throws JSONException {
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_YEAR, -20);
            List<String> ordersList = fHIRCLient.getOrders(fHIRparameters.getGuid(), c.getTime());
            int count = bundleIncomeLoader.syncIncomeOrder(ordersList);
            eventlogFacade.createLogMessage("Успешный импорт входящих заявок " + count, false);
            for (EgiszTargets x : egiszTargetsFacade.loadByOID(fHIRparameters.getOid())) {
                try {
                    ordersList = fHIRCLient.getOrders(x.getCode(), c.getTime());
                    count = bundleIncomeLoader.syncIncomeOrder(ordersList);
                    eventlogFacade.createLogMessage("Успешный импорт входящих заявок " + count, false);
                } catch (WrongNameException ex) {
                    Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (WrongNameException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("/syncoutcomeorders")
    public Response syncOutcomeOrders() throws JSONException {
        try {
            fHIRCLient.syncOrderRequest();
        } catch (Exception ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }


    @GET
    @Produces("application/json")
    @Path("/order/validate/{id}")
    public Response validateOrder(@PathParam("id") Long id) throws JSONException, Exception {
        String res = "";
        try {
            VexlabOutocomeOrders eo = vexlabOutocomeOrdersFacade.find(id);
            if (eo != null) {
                Patient p = fHIRCLient.createPatient(bundleOutcomeLoader.convertPersonsToPatient(peopleFacade.find(eo.getPeopleId())));
                try {
                    fHIRCLient.validateRequest(eo.getMkb(), p.getId().getValue().replaceAll("Order/", ""), eo.getResearchTypes());
                    return Response.status(Response.Status.OK)
                            .type(MediaType.APPLICATION_JSON)
                            .entity("{\"msg\":\"Сервис проверки обоснованности не настроен для выбранной услуги\"}")
                            .build();
                } catch (WrongDiagnException wrongDiagnException) {
                    return Response.status(Response.Status.BAD_REQUEST)
                            .type(MediaType.APPLICATION_JSON)
                            .entity("{\"msg\":\"Диагноз не соответствует услуге\"}")
                            .build();
                } catch (WrongDateIntervalException wrongNameException) {
                    Response.status(Response.Status.BAD_REQUEST)
                            .type(MediaType.APPLICATION_JSON)
                            .entity("{\"msg\":\"У пациента уже есть на направление по данной услуге на выбранную дату\"}")
                            .build();
                }
            }
        } catch (WrongNameException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("/genegiszorders")
    public Response generateNonPreOrders() throws JSONException {
        String res = egiszOrdersFacade.updateBioOrders().toString();
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .entity(res)
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("/order/check")
    public Response checkOrder(@QueryParam("mkb") String mkb, @QueryParam("patid") Long patid, @QueryParam("code") Integer aimId) {
        String res = "";
        try {
            res = fHIRCLient.validateRequest(mkb, patid, aimId);
        } catch (Exception ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
            res = ex.getMessage();
        }
        return Response.status(Response.Status.OK)
                .header("Content-Type", "application/json;charset=UTF-8")
                .entity("{\"msg\":\"" + res + "\"}")
                .build();
    }

}
