/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import ca.uhn.fhir.model.api.ResourceMetadataKeyEnum;
import ca.uhn.fhir.model.base.composite.BaseCodingDt;
import ca.uhn.fhir.model.base.resource.ResourceMetadataMap;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.composite.SimpleQuantityDt;
import ca.uhn.fhir.model.dstu2.resource.Binary;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.DiagnosticReport;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Order;
import ca.uhn.fhir.model.dstu2.resource.OrderResponse;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Practitioner;
import ca.uhn.fhir.model.dstu2.resource.Specimen;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.dstu2.valueset.BundleTypeEnum;
import ca.uhn.fhir.model.dstu2.valueset.DiagnosticReportStatusEnum;
import ca.uhn.fhir.model.dstu2.valueset.HTTPVerbEnum;
import ca.uhn.fhir.model.dstu2.valueset.ObservationStatusEnum;
import ca.uhn.fhir.model.dstu2.valueset.OrderStatusEnum;
import ca.uhn.fhir.model.primitive.DateDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.primitive.InstantDt;
import ca.uhn.fhir.model.primitive.StringDt;
import static com.initmed.controller.util.Util.createCoding;
import static com.initmed.controller.util.Util.createIdt;
import static com.initmed.controller.util.Util.getHumanName;
import com.initmed.db.model.DictDoctors;
import com.initmed.db.model.DocAnalysOrderAim;
import com.initmed.db.model.EgiszOrders;
import com.initmed.db.model.Persons;
import com.initmed.db.model.EgiszOrderitems;
import com.initmed.db.model.VExlabVLabTestResult;
import com.initmed.exceptions.WrongNameException;
import com.initmed.facades.DictDoctorsFacade;
import com.initmed.facades.EgiszOrderItemsFacade;
import com.initmed.facades.EgiszVersionTargets;
import com.initmed.facades.VExlabVLabTestResultFacade;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author sd199
 */
@Stateless
public class BundleBioLoader {

    @EJB
    private FHIRparameters fHIRparameters;
    @EJB
    private EgiszVersionTargets egiszVersionTargets;
    @EJB
    private VExlabVLabTestResultFacade exlabVLabTestResultFacade;
    @EJB
    private DictDoctorsFacade dictDoctorsFacade;

//

    private Practitioner getPerformer(VExlabVLabTestResult vExlabVLabTestResult) throws WrongNameException {
        return convertPersonnelToPractioiner(dictDoctorsFacade.find(vExlabVLabTestResult.getDocid()), vExlabVLabTestResult.getDocdivcode(), vExlabVLabTestResult.getDocspcode().toString(), vExlabVLabTestResult.getDocposcocde().toString());
    }

    private Practitioner getAprover(VExlabVLabTestResult vExlabVLabTestResult) throws WrongNameException {
        return convertPersonnelToPractioiner(dictDoctorsFacade.find(vExlabVLabTestResult.getDocid()), vExlabVLabTestResult.getDocdivcode(), vExlabVLabTestResult.getDocspcode().toString(), vExlabVLabTestResult.getDocposcocde().toString());
    }

    public Bundle convertInBundleNoPreBloodOrder(EgiszOrders order, boolean reload) throws WrongNameException, ParseException {
        Bundle labBundle = new Bundle();
        labBundle.setType(BundleTypeEnum.TRANSACTION);

        ResourceMetadataMap rm = new ResourceMetadataMap();
        rm.putIfAbsent(ResourceMetadataKeyEnum.PROFILES, "StructureDefinition/21f687dd-0b3b-4a7b-af8f-04be625c0201");
        labBundle.setResourceMetadata(rm);
        if (order.getEgiszOrderitemsSet().isEmpty()) {
            throw new WrongNameException("Для указанного EGISZ_EXLAB_ORDERS.ID=" + order.getId().toString() + " отсутствуют EGISZ_EXLAB_ORDERITEMS!");
        }
        List<VExlabVLabTestResult> lst = exlabVLabTestResultFacade.loadResults(order.getEgiszOrderitemsSet().iterator().next().getDocAnalysOrderAim().getId());
        if (lst.isEmpty()) {
            throw new WrongNameException("Для указанного EGISZ_EXLAB_ORDERS.ID=" + order.getId().toString() + " отсутствуют записи результатов VExlabVLabTestResult!");
        }
        VExlabVLabTestResult vExlabVLabTestResult = lst.get(0);
        Persons ps = vExlabVLabTestResult.getPersons();
        Patient pat = convertPersonsToPatient(ps, order.getRefegiszTargets().getCode());
        Practitioner practitioner = getAprover(vExlabVLabTestResult);
        // generate guids
        String orderUUID = "urn:uuid:" + UUID.randomUUID().toString();
        String orderResponseUUID = "urn:uuid:" + UUID.randomUUID().toString();
        ResourceReferenceDt practUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());
        ResourceReferenceDt labPractUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());
        ResourceReferenceDt patUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());

        labBundle.addEntry().setFullUrl(practUUID.getReference()).setResource(practitioner).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Practitioner");
        labBundle.addEntry().setFullUrl(patUUID.getReference()).setResource(pat).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Patient");

        Order od = new Order();
        od.setSource(new ResourceReferenceDt("Organization/" + order.getSource()));
        od.setTarget(new ResourceReferenceDt("Organization/" + order.getRefegiszTargets().getCode()));
        od.addDetail().setReference("123123");

        labBundle.addEntry().setFullUrl(orderUUID).setResource(od).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Order");

        //lab practiocioner
        Practitioner labPractiocioner = getPerformer(vExlabVLabTestResult);
        labBundle.addEntry().setFullUrl(labPractUUID.getReference()).setResource(labPractiocioner).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Practitioner");
        //test

        //create result description
        OrderResponse orderResponse = new OrderResponse();
        IdentifierDt idt = null;
//        if (reload) {
        idt = createIdt(fHIRparameters.getMisOid(), UUID.randomUUID().toString(), null);
//        } else {
//            idt = createIdt(fHIRparameters.getMisOid(), order.getId().toString(), null);
//        }
        orderResponse.addIdentifier(idt);
        orderResponse.setRequest(new ResourceReferenceDt(orderUUID));
        orderResponse.setOrderStatus(OrderStatusEnum.COMPLETED);
        orderResponse.setDate(new DateTimeDt(vExlabVLabTestResult.getAdate()));
        orderResponse.setWho(od.getTarget());
        orderResponse.setDescription("-");
        labBundle.addEntry().setFullUrl(orderResponseUUID).setResource(orderResponse).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("OrderResponse");
        for (EgiszOrderitems orderItem : order.getEgiszOrderitemsSet()) {
            String diagnReportUUID = "urn:uuid:" + UUID.randomUUID().toString();
            String spicemenUUID = "urn:uuid:" + UUID.randomUUID().toString();

            DiagnosticReport diagnosticReport = new DiagnosticReport();
            BaseCodingDt cd = new CodingDt();
            cd.setCode("N");
            ResourceMetadataMap rm1 = new ResourceMetadataMap();
            List<BaseCodingDt> list = new ArrayList<>();
            list.add(cd);
            rm1.put(ResourceMetadataKeyEnum.SECURITY_LABELS, list);
            diagnosticReport.setResourceMetadata(rm1);
            diagnosticReport.setEffective(new DateTimeDt(orderItem.getTakingdate()));
            diagnosticReport.setStatus(DiagnosticReportStatusEnum.FINAL);
//            diagnosticReport.setCategory(createCoding("urn:oid:1.2.643.5.1.13.13.11.1117", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1117").toString(), orderItem.getEgiszResearchGroup().getId().toString()));
            diagnosticReport.setCode(createCoding("urn:oid:1.2.643.2.69.1.1.1.31", egiszVersionTargets.loadVersionByOID("1.2.643.2.69.1.1.1.31").toString(), orderItem.getRefegiszResearchtypes().getCode()));
            diagnosticReport.setIssued(new InstantDt(orderItem.getTakingdate()));
            diagnosticReport.setSubject(patUUID);
            diagnosticReport.setPerformer(practUUID);
            diagnosticReport.addSpecimen().setReference(spicemenUUID);
            diagnosticReport.setConclusion("Заключение");
            if (order.getDiagOrderUUID() != null) {
                diagnosticReport.addRequest().setReference("DiagnosticOrder/" + order.getDiagOrderUUID());
            }
            labBundle.addEntry().setFullUrl(diagnReportUUID).setResource(diagnosticReport).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("DiagnosticReport");
            Specimen sp = new Specimen();
            sp.setType(createCoding("urn:oid:1.2.643.5.1.13.13.11.1081", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1081").toString(), orderItem.getRefegiszMaterials().getId().toString()));
            sp.setSubject(patUUID);
            Specimen.Collection col = new Specimen.Collection();
            col.addComment(order.getDescr());
            col.setCollected(new DateTimeDt(new Date(orderItem.getTakingdate().getTime())));
            sp.setCollection(col);
            Specimen.Container c = new Specimen.Container();
            IdentifierDt idts = new IdentifierDt();
            idts.setSystem("urn:uuid:" + order.getRefegiszTargets().getCode());
            idts.setValue(orderItem.getId().toString());
            c.addIdentifier(idts);
            c.setType(createCoding("urn:oid:1.2.643.2.69.1.1.1.34", "1", String.valueOf(orderItem.getRefegiszContainertypes().getId())));
            sp.addContainer(c);
            labBundle.addEntry().setFullUrl(spicemenUUID).setResource(sp).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Specimen");
            processResult(orderItem, labBundle, diagnosticReport, labPractUUID);
            orderResponse.addFulfillment().setReference(diagnReportUUID);

            Binary binary = new Binary();
            binary.setContentType("application/pdf");
//            binary.setContent((Base64.getEncoder().encode(vExlabVLabTestResult.getDocFile())));
            binary.setContent(vExlabVLabTestResult.getDocFile());
            String binaryUUID = "urn:uuid:" + UUID.randomUUID().toString();
            labBundle.addEntry().setFullUrl(binaryUUID).setResource(binary).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Binary");
            diagnosticReport.addPresentedForm().setUrl(binaryUUID);

            Binary docSign = new Binary();
            docSign.setContentType("application/x-pkcs7-practitioner");
//            docSign.setContent(Base64.getEncoder().encode(vExlabVLabTestResult.getDocsign()));
            docSign.setContent(vExlabVLabTestResult.getDocsign());
            String docSignUUID = "urn:uuid:" + UUID.randomUUID().toString();
            labBundle.addEntry().setFullUrl(docSignUUID).setResource(docSign).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Binary");
            diagnosticReport.addPresentedForm().setUrl(docSignUUID);

            Binary orgSign = new Binary();
            orgSign.setContentType("application/x-pkcs7-organization");
//            orgSign.setContent(Base64.getEncoder().encode(vExlabVLabTestResult.getOrgsign()));
            orgSign.setContent(vExlabVLabTestResult.getOrgsign());
            String orgSignUUID = "urn:uuid:" + UUID.randomUUID().toString();
            labBundle.addEntry().setFullUrl(orgSignUUID).setResource(orgSign).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Binary");
            diagnosticReport.addPresentedForm().setUrl(orgSignUUID);
            break;
        }
        return labBundle;
    }

    private Bundle processResult(EgiszOrderitems itemOrder, Bundle labBundle, DiagnosticReport diagnosticReport, ResourceReferenceDt labPractiocioner) {
        for (VExlabVLabTestResult x : exlabVLabTestResultFacade.loadResults(itemOrder.getDocAnalysOrderAim().getId())) {
            String obsTestUUID = "urn:uuid:" + UUID.randomUUID().toString();
            Double max = x.getMinVal();
            Double min = x.getMaxVal();
            String resStr = x.getResDigit() == null ? x.getResText() : x.getResDigit().toString();
            Double res = null;
            try {
                res = Double.parseDouble(resStr.replaceAll(",", "."));
                String measureId = x.getEgiszMeasure().getId().toString();
                String measureTypeId = x.getRefMeasureType().getId().toString();
                Observation obsTest = createNumbersObs(labPractiocioner, measureTypeId, x.getAdate(), measureId, res, min, max);
                labBundle.addEntry().setFullUrl(obsTestUUID).setResource(obsTest).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Observation");
                diagnosticReport.addResult().setReference(obsTestUUID);
            } catch (Exception e) {
                Observation o = createQualObs(labPractiocioner, "1126424", x.getAdate(), resStr);
                labBundle.addEntry().setFullUrl(obsTestUUID).setResource(o).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Observation");
                diagnosticReport.addResult().setReference(obsTestUUID);
            }

        }
        return labBundle;
    }

    private Patient convertPersonsToPatient(Persons pers, String idLPu) throws WrongNameException {
        Patient pat = new Patient();
        //create idt for mis
        pat.addIdentifier(createIdt("urn:oid:1.2.643.5.1.13.2.7.100.5", "MS_P_ID" + pers.getId(), fHIRparameters.getMisOid()));
        //snils idt
        if (pers.getSnils() != null) {
            pat.addIdentifier(createIdt("urn:oid:1.2.643.2.69.1.1.1.6.223", pers.getSnils().replaceAll("\\D", ""), "ПФР"));
        }
//        //pasp idt
//        if (pers.getRefdocumenttypes() == 21 && pers.getDocumentno() != null && pers.getDocdeliver() != null) {
//            pat.addIdentifier(createIdt("urn:oid:1.2.643.2.69.1.1.1.6.14", pers.getDocumentno().replace(' ', ':'), pers.getDocdeliver(), pers.getDeliverydate(), null));
//        }
        pat.setName(getHumanName(pers));
        pat.setGender(pers.getSex() == 1 ? AdministrativeGenderEnum.MALE : AdministrativeGenderEnum.FEMALE);
        pat.setBirthDate(new DateDt(pers.getBirthDate()));
        pat.setManagingOrganization(new ResourceReferenceDt("Organization/" + idLPu));
        return pat;
    }

    private Observation createNumbersObs(ResourceReferenceDt pr, String resultMarkId, Date dat, String measureCode, Double res, Double min, Double max) {
        String resRefrence = res > max ? "H" : "N";
        if (min != null && res < min) {
            resRefrence = "L";
        }
        return createNumbersObs(pr, resultMarkId, resRefrence, dat, measureCode, res, min, max);
    }

    private Observation createNumbersObs(ResourceReferenceDt pr, String resultMarkId, String resRefrence, Date dat, String measureCode, Double res, Double min, Double max) {
        Observation obsTest = new Observation();
        obsTest.setCode(createCoding("urn:oid:1.2.643.5.1.13.13.11.1080", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1080").toString(), resultMarkId));

        obsTest.setInterpretation(createCoding("urn:oid:1.2.643.5.1.13.13.11.1381", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1381").toString(), resRefrence));
        obsTest.setStatus(ObservationStatusEnum.FINAL);
        obsTest.setIssuedWithMillisPrecision(dat);

        QuantityDt qd = new QuantityDt();
        qd.setCode(measureCode);
        qd.setValue(res);
        obsTest.setValue(qd);

        Observation.ReferenceRange r = obsTest.addReferenceRange();
        if (min != null) {
            SimpleQuantityDt low = new SimpleQuantityDt();
            low.setValue(min);
            low.setCode(measureCode);
            r.setLow(low);
        }
        if (max != null) {
            SimpleQuantityDt high = new SimpleQuantityDt();
            high.setValue(max);
            high.setCode(measureCode);
            r.setHigh(high);
        }
        obsTest.setValue(qd);
        obsTest.setComments("");
        obsTest.addPerformer().setReference(pr.getReference().getValue());
        return obsTest;
    }

    private Observation createQualObs(ResourceReferenceDt pr, String resultMarkId, Date dat, String res) {
        Observation obsTest = new Observation();
        obsTest.setCode(createCoding("urn:oid:1.2.643.5.1.13.13.11.1080", 
                egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1080").toString(), resultMarkId));

        if (res.trim().toLowerCase().equals("обнаружено")) {
            obsTest.setInterpretation(createCoding("urn:oid:1.2.643.5.1.13.13.11.1381", 
                    egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1381").toString(), "DET"));
        } else {
            obsTest.setInterpretation(createCoding("urn:oid:1.2.643.5.1.13.13.11.1381", 
                    egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1381").toString(), "ND"));
        }
        obsTest.setStatus(ObservationStatusEnum.FINAL);
        obsTest.setIssuedWithMillisPrecision(dat);

        Observation.ReferenceRange rNeg = obsTest.addReferenceRange();
        rNeg.setText("Не обнаружено, порог чувствительности");
        StringDt valNeg = new StringDt(res);
        obsTest.setValue(valNeg);
        obsTest.setComments("");
        obsTest.addPerformer().setReference(pr.getReference().getValue());
        return obsTest;
    }

//    private byte[] createPdf(Map<String, String> dat, EgiszOrderitems itemOrder) throws IOException {
//        Persons ps = getAprover().getPersons();
//        return pdfConverter.createPdf(dat, ps.getLastName() + " " + ps.getLastName() + " " + ps.getMiddleName(), itemOrder.getTakingdate());
//    }
//
//    private Observation createLettersObs(ResourceReferenceDt pr, String resultMarkId, Date dat, String res) {
//        Observation obsTest = new Observation();
//        obsTest.setCode(createCoding("1.2.643.5.1.13.13.11.1080", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1080").toString(), resultMarkId));
//
//        obsTest.setInterpretation(createCoding("1.2.643.5.1.13.13.11.1381", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1381").toString(), "DET"));
//        obsTest.setStatus(ObservationStatusEnum.FINAL);
//        obsTest.setIssuedWithMillisPrecision(dat);
//
//        Observation.ReferenceRange r = obsTest.addReferenceRange();
//        r.setText("Описание");
//        StringDt val = new StringDt(res);
//        obsTest.setValue(val);
//
//        obsTest.setComments("");
//        obsTest.addPerformer().setReference(pr.getReference().getValue());
//        return obsTest;
//    }
    public Bundle convertInResultBundle(EgiszOrders order) throws WrongNameException {
        Bundle labBundle = new Bundle();
        labBundle.setType(BundleTypeEnum.TRANSACTION);
        ResourceMetadataMap rm = new ResourceMetadataMap();
        rm.putIfAbsent(ResourceMetadataKeyEnum.PROFILES, "StructureDefinition/21f687dd-0b3b-4a7b-af8f-04be625c0201");
        labBundle.setResourceMetadata(rm);
//        Persons ps = exlabVLabTestResultFacade.loadResults(order.getEgiszOrderitemsSet().iterator().next().getDocAnalysOrderAim().getId()).get(0).getPersons();
        if (order.getEgiszOrderitemsSet().isEmpty()) {
            throw new WrongNameException("Для указанного EGISZ_EXLAB_ORDERS.ID=" + order.getId().toString() + " отсутствуют EGISZ_EXLAB_ORDERITEMS!");
        }
        List<VExlabVLabTestResult> lst = exlabVLabTestResultFacade.loadResults(order.getEgiszOrderitemsSet().iterator().next().getDocAnalysOrderAim().getId());
        if (lst.isEmpty()) {
            throw new WrongNameException("Для указанного EGISZ_EXLAB_ORDERS.ID=" + order.getId().toString() + " отсутствуют записи результатов VExlabVLabTestResult!");
        }
        VExlabVLabTestResult vExlabVLabTestResult = lst.get(0);
        Practitioner practitioner = getAprover(vExlabVLabTestResult);
        // generate guids
        String orderUUID = "Order/" + order.getOrederUUID();
        String orderResponseUUID = "urn:uuid:" + UUID.randomUUID().toString();
        ResourceReferenceDt practUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());
        ResourceReferenceDt labPractUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());
        ResourceReferenceDt patUUID = new ResourceReferenceDt(order.getRefpersons().getPersonsUUID());

        labBundle.addEntry().setFullUrl(practUUID.getReference()).setResource(practitioner).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Practitioner");

        //create result description
        OrderResponse orderResponse = new OrderResponse();
        IdentifierDt idt = createIdt(fHIRparameters.getMisOid(), order.getId().toString(), null);
        orderResponse.addIdentifier(idt);
        orderResponse.setRequest(new ResourceReferenceDt(orderUUID));
        orderResponse.setOrderStatus(OrderStatusEnum.COMPLETED);
        orderResponse.setDate(new DateTimeDt(exlabVLabTestResultFacade.loadResults(order.getEgiszOrderitemsSet().iterator().next().getDocAnalysOrderAim().getId()).get(0).getAdate()));
        orderResponse.setWho(new ResourceReferenceDt("Organization/" + order.getRefegiszTargets().getCode()));
        orderResponse.setDescription("-");

        labBundle.addEntry().setFullUrl(orderResponseUUID).setResource(orderResponse).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("OrderResponse");

        //lab practiocioner
        Practitioner labPractiocioner = getPerformer(vExlabVLabTestResult);
        labBundle.addEntry().setFullUrl(labPractUUID.getReference()).setResource(labPractiocioner).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Practitioner");

        for (EgiszOrderitems x : order.getEgiszOrderitemsSet()) {
            VExlabVLabTestResult r = exlabVLabTestResultFacade.loadResults(x.getDocAnalysOrderAim().getId()).get(0);

            DiagnosticReport diagnosticReport = new DiagnosticReport();
            BaseCodingDt cd = new CodingDt();
            cd.setCode("N");
            ResourceMetadataMap rm1 = new ResourceMetadataMap();
            List<BaseCodingDt> list = new ArrayList<>();
            list.add(cd);
            rm1.put(ResourceMetadataKeyEnum.SECURITY_LABELS, list);
            diagnosticReport.setResourceMetadata(rm1);
            diagnosticReport.setEffective(new DateTimeDt(r.getAdate()));
            diagnosticReport.setStatus(DiagnosticReportStatusEnum.FINAL);
            diagnosticReport.setCategory(createCoding("urn:oid:1.2.643.5.1.13.13.11.1117", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1117").toString(), x.getEgiszResearchGroup().getId().toString()));
            diagnosticReport.setCode(createCoding("urn:oid:1.2.643.2.69.1.1.1.31", egiszVersionTargets.loadVersionByOID("1.2.643.2.69.1.1.1.31").toString(), x.getRefegiszResearchtypes().getCode()));
            diagnosticReport.setIssued(new InstantDt(r.getAdate()));
            diagnosticReport.setSubject(patUUID);
            if (x.getSpicimenId() != null) {
                diagnosticReport.addSpecimen().setReference(x.getSpicimenId());
            }
            diagnosticReport.setPerformer(practUUID);
            diagnosticReport.setConclusion("Заключение");
            diagnosticReport.addRequest().setReference(x.getDiagnOrderId());
            String diagnReportUUID = "urn:uuid:" + UUID.randomUUID().toString();
            labBundle.addEntry().setFullUrl(diagnReportUUID).setResource(diagnosticReport).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("DiagnosticReport");
            orderResponse.addFulfillment().setReference(diagnReportUUID);
            //test
            processResult(x, labBundle, diagnosticReport, labPractUUID);

            Binary binary = new Binary();
            binary.setContentType("application/pdf");
//            binary.setContent((Base64.getEncoder().encode(vExlabVLabTestResult.getDocFile())));
            binary.setContent(vExlabVLabTestResult.getDocFile());
            String binaryUUID = "urn:uuid:" + UUID.randomUUID().toString();
            labBundle.addEntry().setFullUrl(binaryUUID).setResource(binary).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Binary");
            diagnosticReport.addPresentedForm().setUrl(binaryUUID);

            Binary docSign = new Binary();
            docSign.setContentType("application/x-pkcs7-practitioner");
//            docSign.setContent(Base64.getEncoder().encode(vExlabVLabTestResult.getDocsign()));
            docSign.setContent(vExlabVLabTestResult.getDocsign());
            String docSignUUID = "urn:uuid:" + UUID.randomUUID().toString();
            labBundle.addEntry().setFullUrl(docSignUUID).setResource(docSign).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Binary");
            diagnosticReport.addPresentedForm().setUrl(docSignUUID);

            Binary orgSign = new Binary();
            orgSign.setContentType("application/x-pkcs7-organization");
//            orgSign.setContent(Base64.getEncoder().encode(vExlabVLabTestResult.getOrgsign()));
            orgSign.setContent(vExlabVLabTestResult.getOrgsign());
            String orgSignUUID = "urn:uuid:" + UUID.randomUUID().toString();
            labBundle.addEntry().setFullUrl(orgSignUUID).setResource(orgSign).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Binary");
            diagnosticReport.addPresentedForm().setUrl(orgSignUUID);
        }
        return labBundle;
    }

    private Practitioner convertPersonnelToPractioiner(DictDoctors doc, String idLpu, String spec, String pos) throws WrongNameException {
        Practitioner pract = new Practitioner();
        pract.addIdentifier(createIdt("urn:oid:1.2.643.5.1.13.2.7.100.5", "MS_DOC_ID" + doc.getId(), fHIRparameters.getMisOid()));
        //create idt for mis
        //snils idt
        if (doc.getSnils() != null) {
            pract.addIdentifier(createIdt("urn:oid:1.2.643.2.69.1.1.1.6.223", doc.getSnils().replaceAll("\\D", ""), "ПФР"));
        }
        pract.setName(getHumanName(doc.getFio()).get(0));
        Practitioner.PractitionerRole pr = new Practitioner.PractitionerRole();
        pr.setManagingOrganization(new ResourceReferenceDt("Organization/" + idLpu));
        pr.addSpecialty(createCoding("urn:oid:1.2.643.5.1.13.13.11.1066", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1066").toString(), spec));
        pr.setRole(createCoding("urn:oid:1.2.643.5.1.13.13.11.1002", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1002").toString(), pos));
        pract.addPractitionerRole(pr);
//        pract.setGender();
//        pract.setBirthDate(doc.getBi);
        pract.setActive(Boolean.TRUE);
        return pract;
    }
}
