/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import ca.uhn.fhir.model.dstu2.resource.Condition;
import ca.uhn.fhir.model.dstu2.resource.DiagnosticOrder;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Order;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Specimen;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import com.initmed.db.model.DocAnalysOrder;
import com.initmed.db.model.DocAnalysOrderAim;
import com.initmed.db.model.EgiszContainertypes;
import com.initmed.db.model.EgiszMaterials;
import com.initmed.db.model.EgiszTargets;
import com.initmed.db.model.TempPersons;
import com.initmed.db.model.EgiszOrders;
import com.initmed.db.model.EgiszOrderitems;
import com.initmed.db.model.EgiszResearchtypes;
import com.initmed.facades.DocAnalysOrderAimFacade;
import com.initmed.facades.DocAnalysOrderFacade;
import com.initmed.facades.EgiscontainertypesFacade;
import com.initmed.facades.EgiszOrderItemsFacade;
import com.initmed.facades.EgiszOrdersFacade;
import com.initmed.facades.EgiszTargetsFacade;
import com.initmed.facades.EgiszresearchmaterialsFacade;
import com.initmed.facades.EgiszresearchtypesFacade;
import com.initmed.facades.EventlogFacade;
import com.initmed.facades.TempPersonsFacade;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author sd199
 */
@Stateless
public class BundleIncomeLoader {

    @EJB
    private EgiszOrdersFacade egiszOrdersFacade;
    @EJB
    private EgiszTargetsFacade egiszTargetsFacade;
    @EJB
    private FHIRCLient fHIRCLient;
    @EJB
    private EventlogFacade eventlogFacade;
    @EJB
    private TempPersonsFacade tempPersonsFacade;
    @EJB
    private EgiszOrderItemsFacade egiszOrderItemsFacade;
    @EJB
    private EgiszresearchtypesFacade refEgiszresearchtypesFacade;
    @EJB
    private EgiszresearchmaterialsFacade refEgiszresearchmaterialsFacade;
    @EJB
    private EgiscontainertypesFacade egiscontainertypesFacade;
    @EJB
    private DocAnalysOrderAimFacade docAnlysAims;
    @EJB
    private DocAnalysOrderFacade docAnalysOrderFacade;

    public Integer syncIncomeOrder(List<String> incomeOrderId) {
        int count = 0;
        for (String x : incomeOrderId) {
            EgiszOrders e = egiszOrdersFacade.getOrderByGUID(x.trim());
            if (e == null) {
                try {
                    Order o = fHIRCLient.getOrder("Order/" + x);
                    Patient p = fHIRCLient.getPatient(o.getSubject().getReference().getValue());
                    List<DiagnosticOrder> dOrderLits = fHIRCLient.getDiagnosticOrder(o.getDetail());
                    Encounter ec = fHIRCLient.getEncounter(dOrderLits.iterator().next().getEncounter().getReference().getValue());
                    Condition c = fHIRCLient.getCondition(ec.getIndication().get(0).getReference().getValue());

                    TempPersons tp = tempPersonsFacade.syncPatient(p);

                    EgiszOrders eo = new EgiszOrders();
                    eo.setIncome((short) 1);
                    eo.setCreatetime(new Date());
                    EgiszTargets targets = egiszTargetsFacade.find(o.getTarget().getReference().getValue().replace("Organization/", ""));
                    eo.setSource(o.getIdentifierFirstRep().getAssigner().getReference().getValue().replace("Organization/", ""));
                    eo.setRefegiszTargets(targets);
                    eo.setOrederUUID(x);
                    eo.setDiagnosisdate(c.getDateRecorded());
                    eo.setMkb(c.getCode().getCoding().get(0).getCode());
                    eo.setRefpersons(tp);
                    eo.setProcessedindb((short) 1);
                    String orderId = dOrderLits.get(0).getId().getValue().replace("DiagnosticOrder/", "");
                    eo.setDiagOrderUUID(orderId.substring(0, orderId.indexOf("/_")));
                    e = egiszOrdersFacade.getOrderByDiagnOrderUUID(eo.getDiagOrderUUID());
                    if (e == null) {
                        DocAnalysOrder docOrder = new DocAnalysOrder();
                        docOrder.setDate(o.getDate());
                        docOrder.setPeople(tp.getIntermedId());
                        docOrder.setPayed((short) 0);
                        docOrder.setDocId(docAnalysOrderFacade.calculateDoc(o.getTarget().getReference().getValue().replace("Organization/", "")));
                        docOrder.setSum(0.0);
                        docOrder.setTransfered((short) 0);
                        eo.setDocAnalysOrder(docOrder);
                        docAnalysOrderFacade.create(docOrder);
                        egiszOrdersFacade.create(eo);
                        for (DiagnosticOrder d : dOrderLits) //creating order
                        {

                            Specimen sp = null;
                            if (!d.getSpecimen().isEmpty()) {
                                sp = fHIRCLient.getSpecimen(d.getSpecimen().get(0).getReference().getValue());
                            }
                            for (DiagnosticOrder.Item i : d.getItem()) {
                                String spId = null;
                                if (sp != null) {
                                    spId = sp.getId().getValue();
                                }
                                if ((spId != null) && !spId.isEmpty()) {
                                    spId = spId.substring(0, spId.indexOf("/_"));
                                } 
                                String doId = d.getId().getValue();
                                doId = doId.substring(0, doId.indexOf("/_"));

                                EgiszOrderitems item = new EgiszOrderitems();
                                item.setRefegiszOrders(eo);
                                EgiszResearchtypes researType = refEgiszresearchtypesFacade.find(i.getCode().getCoding().get(0).getCode());
                                item.setRefegiszResearchtypes(researType);
                                item.setDiagnOrderId(doId);
                                item.setSpicimenId(spId);
                                EgiszMaterials em = null;
                                if (sp != null) {
                                    em = refEgiszresearchmaterialsFacade.find(Integer.parseInt(sp.getType().getCodingFirstRep().getCode()));
                                }
                                if (em == null) {
                                    em = refEgiszresearchmaterialsFacade.find(141);
                                }
                                item.setRefegiszMaterials(em);
                                Integer cType = 1;
                                if (sp != null
                                        && sp.getContainer() != null && !sp.getContainer().isEmpty() && sp.getContainer().get(0) != null && sp.getContainer().get(0).getType() != null
                                        && sp.getContainer().get(0).getType().getCodingFirstRep() != null
                                        && sp.getContainer().get(0).getType().getCodingFirstRep().getCode() != null) {
                                    cType = Integer.parseInt(sp.getContainer().get(0).getType().getCodingFirstRep().getCode());
                                }
                                EgiszContainertypes containerType = egiscontainertypesFacade.find(cType);
                                item.setRefegiszContainertypes(containerType);
                                if (sp != null) {
                                    item.setTakingdate(((DateTimeDt) sp.getCollection().getCollected()).getValue());
                                } else {
                                    item.setTakingdate(docOrder.getDate());
                                }
                                egiszOrderItemsFacade.create(item);

                                Integer research = docAnalysOrderFacade.getResearch(i.getCode().getCoding().get(0).getCode());
                                Integer material = docAnalysOrderFacade.getMaterial(i.getCode().getCoding().get(0).getCode(), em.getId());
                                if (research != null && material != null) {
                                    DocAnalysOrderAim docAnalysOrderAim = new DocAnalysOrderAim();
                                    docAnalysOrderAim.setMaterial(material);
                                    docAnalysOrderAim.setAnalysAim(research);
                                    docAnalysOrderAim.setRefAnalysOrder(docOrder);
                                    docAnalysOrderAim.setStatusID((short) 4);
                                    docAnalysOrderAim.setTransfered((short) 0);
                                    item.setDocAnalysOrderAim(docAnalysOrderAim);
                                    docAnlysAims.create(docAnalysOrderAim);
                                    egiszOrderItemsFacade.edit(item);
                                } else {
                                    eo.setProcessedindb((short) 0);
                                    egiszOrdersFacade.edit(eo);
                                    eventlogFacade.createLogMessage("Перекодировка для типа услуги " + i.getCode().getCoding().get(0).getCode() + " и материалла " + sp.getType().getCodingFirstRep().getCode() + " не найдена", false);
                                }
                            }
                        }
                    }
                    count++;
                } catch (Exception ex) {
                    eventlogFacade.createLogMessage(ex.getMessage(), true);
                    Logger.getLogger(BundleIncomeLoader.class.getName()).log(Level.SEVERE, null, ex);
                }

            } 
            /*
            else if (e.getDiagOrderUUID() == null) {
                Order o;
                try {
                    o = fHIRCLient.getOrder("Order/" + x);
                    List<DiagnosticOrder> dOrderLits = fHIRCLient.getDiagnosticOrder(o.getDetail());
                    String orderId = dOrderLits.get(0).getId().getValue().replace("DiagnosticOrder/", "");
                    e.setDiagOrderUUID(orderId.substring(0, orderId.indexOf("/_")));
                } catch (Exception ex) {
                    Logger.getLogger(BundleIncomeLoader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }*/
        }
        return count;
    }
}
