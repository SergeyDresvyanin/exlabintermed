/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import ca.uhn.fhir.model.api.ExtensionDt;
import ca.uhn.fhir.model.api.ResourceMetadataKeyEnum;
import ca.uhn.fhir.model.base.composite.BaseCodingDt;
import ca.uhn.fhir.model.base.resource.ResourceMetadataMap;
import ca.uhn.fhir.model.dstu2.composite.BoundCodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Condition;
import ca.uhn.fhir.model.dstu2.resource.DiagnosticOrder;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Order;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Practitioner;
import ca.uhn.fhir.model.dstu2.resource.Specimen;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.dstu2.valueset.BundleTypeEnum;
import ca.uhn.fhir.model.dstu2.valueset.ConditionCategoryCodesEnum;
import ca.uhn.fhir.model.dstu2.valueset.ConditionVerificationStatusEnum;
import ca.uhn.fhir.model.dstu2.valueset.DiagnosticOrderStatusEnum;
import ca.uhn.fhir.model.dstu2.valueset.EncounterClassEnum;
import ca.uhn.fhir.model.dstu2.valueset.EncounterStateEnum;
import ca.uhn.fhir.model.dstu2.valueset.HTTPVerbEnum;
import ca.uhn.fhir.model.primitive.DateDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import static com.initmed.controller.util.Util.createCoding;
import static com.initmed.controller.util.Util.createIdt;
import static com.initmed.controller.util.Util.getHumanName;
import com.initmed.db.model.Personnnel;
import com.initmed.db.model.Persons;
import com.initmed.db.model.VexlabOutocomeOrders;
import com.initmed.exceptions.WrongNameException;
import com.initmed.facades.EgiszVersionTargets;
import com.initmed.facades.PersonsFacade;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author sd199
 */
@Stateless
public class BundleOutcomeLoader {

    @EJB
    private EgiszVersionTargets egiszVersionTargets;
    @EJB
    private FHIRparameters fHIRparameters;
    @EJB
    private PersonsFacade peopleFacade;

    public Bundle convertOrederBundle(VexlabOutocomeOrders order) throws WrongNameException, ParseException {
        Bundle labBundle = new Bundle();
        labBundle.setType(BundleTypeEnum.TRANSACTION);

        ResourceMetadataMap rm = new ResourceMetadataMap();
        rm.putIfAbsent(ResourceMetadataKeyEnum.PROFILES, "StructureDefinition/cd45a667-bde0-490f-b602-8d780acf4aa2");
        labBundle.setResourceMetadata(rm);

        Patient pat = convertPersonsToPatient(peopleFacade.find(order.getPeopleId()));

        Persons ps = new Persons();
        ps.setLastName(order.getDocLastName());
        ps.setFirstName(order.getDocFirstName());
        ps.setMiddleName(order.getDocMiddleName());
        ps.setSex((short) 0);
        ps.setSnils(order.getDocSnils());
        ps.setSnils("153-308-400-31");
        ps.setId((long) 1);
        Personnnel pl = new Personnnel(ps, 158, 13);

        Practitioner practitioner = convertPersonnelToPractioiner(pl);
        // generate guids
        String orderUUID = "urn:uuid:" + UUID.randomUUID().toString();
        String diagnOrderUUID = "urn:uuid:" + UUID.randomUUID().toString();
        String spicemenUUID = "urn:uuid:" + UUID.randomUUID().toString();
        ResourceReferenceDt practUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());
        ResourceReferenceDt patUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());
        ResourceReferenceDt encounterUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());
        ResourceReferenceDt conditionUUID = new ResourceReferenceDt("urn:uuid:" + UUID.randomUUID().toString());

        labBundle.addEntry().setFullUrl(practUUID.getReference()).setResource(practitioner).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Practitioner");
        labBundle.addEntry().setFullUrl(patUUID.getReference()).setResource(pat).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Patient");

        Order od = new Order();
        IdentifierDt idt = createIdt(fHIRparameters.getMisOid(), order.getId().toString(), null);
        idt.setAssigner(new ResourceReferenceDt("Organization/" + fHIRparameters.getGuid()));
        od.addIdentifier(idt);
        od.setDate(new DateTimeDt(order.getCollectDate()));
        od.setSubject(patUUID);
        od.setSource(practUUID);
        od.setTarget(new ResourceReferenceDt("Organization/" + order.getOrg_Guid()));
        Order.When w = new Order.When();
        w.setCode(createCoding("1.2.643.2.69.1.1.1.30", egiszVersionTargets.loadVersionByOID("1.2.643.2.69.1.1.1.30").toString(), "Routine"));
        od.setWhen(w);
        od.addDetail().setReference(diagnOrderUUID);

        labBundle.addEntry().setFullUrl(orderUUID).setResource(od).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Order");

        DiagnosticOrder diagnosticOrder = new DiagnosticOrder();
        BaseCodingDt cd = new CodingDt();
        cd.setCode("N");
        ResourceMetadataMap rm1 = new ResourceMetadataMap();
        List<BaseCodingDt> list = new ArrayList<>();
        list.add(cd);

        rm1.put(ResourceMetadataKeyEnum.SECURITY_LABELS, list);
        diagnosticOrder.setResourceMetadata(rm1);
        diagnosticOrder.setSubject(patUUID);
        diagnosticOrder.setOrderer(practUUID);
        diagnosticOrder.setEncounter(encounterUUID);
        diagnosticOrder.addSpecimen().setReference(spicemenUUID);
        diagnosticOrder.setStatus(DiagnosticOrderStatusEnum.REQUESTED);
        diagnosticOrder.setItem(convertEgiszItems(order));
        labBundle.addEntry().setFullUrl(diagnOrderUUID).setResource(diagnosticOrder).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("DiagnosticOrder");

        Specimen sp = new Specimen();
        sp.setType(createCoding("urn:oid:1.2.643.5.1.13.13.11.1081", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1081").toString(), order.getRefMaterials().toString()));
        sp.setSubject(patUUID);
        Specimen.Collection col = new Specimen.Collection();
        col.addComment(" ");
        col.setCollected(new DateTimeDt(order.getCollectDate()));
        sp.setCollection(col);
        Specimen.Container c = new Specimen.Container();
        IdentifierDt idts = new IdentifierDt();
        idts.setSystem("urn:uuid:" + order.getOrg_Guid());
        idts.setValue(order.getId().toString());
        c.addIdentifier(idts);
        c.setType(createCoding("urn:oid:1.2.643.2.69.1.1.1.34", "1", String.valueOf(order.getRefContainertypes())));
        sp.addContainer(c);
        labBundle.addEntry().setFullUrl(spicemenUUID).setResource(sp).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Specimen");

        Encounter encounter = new Encounter();
        encounter.addIdentifier(createIdt(fHIRparameters.getMisOid(), order.getId().toString(), null));
        encounter.setStatus(EncounterStateEnum.IN_PROGRESS);
        encounter.setClassElement(EncounterClassEnum.AMBULATORY);
        encounter.addType(createCoding("1.2.643.2.69.1.1.1.35", egiszVersionTargets.loadVersionByOID("1.2.643.2.69.1.1.1.35").toString(), "1"));
        encounter.setPatient(patUUID);
        encounter.addIndication().setReference(conditionUUID.getReference());
        encounter.setServiceProvider(new ResourceReferenceDt("Organization/" + fHIRparameters.getGuid()));
        labBundle.addEntry().setFullUrl(encounterUUID.getReference()).setResource(encounter).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Encounter");

        Condition condition = new Condition();
        condition.setPatient(patUUID);
        BoundCodeableConceptDt<ConditionCategoryCodesEnum> b = new BoundCodeableConceptDt<ConditionCategoryCodesEnum>();
        CodingDt conditionCatCoding = b.addCoding();
        conditionCatCoding.setSystem("1.2.643.2.69.1.1.1.36");
        conditionCatCoding.setVersion(egiszVersionTargets.loadVersionByOID("1.2.643.2.69.1.1.1.36").toString());
        conditionCatCoding.setCode("diagnosis");
        condition.setCategory(b);
        condition.setDateRecorded(new DateDt(order.getDiagndate()));
        condition.setCode(createCoding("1.2.643.2.69.1.1.1.2", egiszVersionTargets.loadVersionByOID("1.2.643.2.69.1.1.1.2").toString(), order.getMkb()));
        condition.setVerificationStatus(ConditionVerificationStatusEnum.CONFIRMED);
        labBundle.addEntry().setFullUrl(conditionUUID.getReference()).setResource(condition).getRequest().setMethod(HTTPVerbEnum.POST).setUrl("Condition");
        return labBundle;
    }

    private List<DiagnosticOrder.Item> convertEgiszItems(VexlabOutocomeOrders order) {
        List<DiagnosticOrder.Item> items = new ArrayList<>();
        DiagnosticOrder.Item item = new DiagnosticOrder.Item();
        item.setCode(createCoding("urn:oid:1.2.643.2.69.1.1.1.31", egiszVersionTargets.loadVersionByOID("1.2.643.2.69.1.1.1.31").toString(), order.getResearchTypes()));
        ExtensionDt ext = new ExtensionDt();
        ext.setUrl("urn:oid:1.2.643.2.69.1.100.1");
        ext.setValue(createCoding("urn:oid:1.2.643.2.69.1.1.1.32", "1", order.getPaymenttype()));
        item.getCode().addUndeclaredExtension(ext);
        items.add(item);
        return items;
    }

    public Patient convertPersonsToPatient(Persons pers) throws WrongNameException {
        Patient pat = new Patient();
        //create idt for mis
        pat.addIdentifier(createIdt("urn:oid:1.2.643.5.1.13.2.7.100.5", "MS_P_ID" + pers.getId(), fHIRparameters.getMisOid()));
        //snils idt
        if (pers.getSnils() != null) {
            pat.addIdentifier(createIdt("urn:oid:1.2.643.2.69.1.1.1.6.223", pers.getSnils().replaceAll("\\D", ""), "ПФР"));
        }
//        //pasp idt
//        if (pers.getRefdocumenttypes() == 21 && pers.getDocumentno() != null && pers.getDocdeliver() != null) {
//            pat.addIdentifier(createIdt("urn:oid:1.2.643.2.69.1.1.1.6.14", pers.getDocumentno().replace(' ', ':'), pers.getDocdeliver(), pers.getDeliverydate(), null));
//        }
        pat.setName(getHumanName(pers));
        pat.setGender(pers.getSex() == 1 ? AdministrativeGenderEnum.MALE : AdministrativeGenderEnum.FEMALE);
        pat.setBirthDate(new DateDt(pers.getBirthDate()));
        pat.setManagingOrganization(new ResourceReferenceDt("Organization/" + fHIRparameters.getGuid()));
        return pat;
    }

    private Practitioner convertPersonnelToPractioiner(Personnnel doc) throws WrongNameException {
        Persons pers = doc.getPersons();
        Practitioner pract = new Practitioner();
        pract.addIdentifier(createIdt("urn:oid:1.2.643.5.1.13.2.7.100.5", "MS_DOC_ID" + pers.getId(), fHIRparameters.getMisOid()));
        //create idt for mis
        //snils idt
        if (pers.getSnils() != null) {
            pract.addIdentifier(createIdt("urn:oid:1.2.643.2.69.1.1.1.6.223", pers.getSnils().replaceAll("\\D", ""), "ПФР"));
        }
        pract.setName(getHumanName(pers).get(0));
        pract.setGender(pers.getSex() == 1 ? AdministrativeGenderEnum.MALE : AdministrativeGenderEnum.FEMALE);
        if (pers.getBirthDate() != null) {
            pract.setBirthDate(new DateDt(pers.getBirthDate()));
        }
        Practitioner.PractitionerRole pr = new Practitioner.PractitionerRole();
        pr.setManagingOrganization(new ResourceReferenceDt("Organization/" + fHIRparameters.getGuid()));
        pr.addSpecialty(createCoding("urn:oid:1.2.643.5.1.13.13.11.1066", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1066").toString(), doc.getSpeciality().toString()));
        pr.setRole(createCoding("urn:oid:1.2.643.5.1.13.13.11.1002", egiszVersionTargets.loadVersionByOID("1.2.643.5.1.13.13.11.1002").toString(), doc.getPosition().toString()));
        pract.addPractitionerRole(pr);
        return pract;
    }

}
