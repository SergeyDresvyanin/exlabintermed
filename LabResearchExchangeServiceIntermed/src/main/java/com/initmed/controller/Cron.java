/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import com.initmed.db.model.EgiszTargets;
import com.initmed.exceptions.WrongNameException;
import com.initmed.facades.EgiszOrdersFacade;
import com.initmed.facades.EgiszTargetsFacade;
import com.initmed.facades.EventlogFacade;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author sd199
 */
@Startup
@Singleton
public class Cron {

    @EJB
    private EgiszOrdersFacade egiszOrdersFacade;
    @EJB
    private FHIRCLient fHIRCLient;
    @EJB
    private EventlogFacade eventlogFacade;
    @EJB
    private BundleIncomeLoader bundleIncomeLoader;
    @EJB
    private FHIRparameters fHIRparameters;
    @EJB
    private EgiszTargetsFacade egiszTargetsFacade;

//    @Schedule(minute = "*/5", hour = "*", persistent = false)
    @Schedule(hour = "*/1", minute = "0", second = "0", persistent = false)
    public void updateEgiszOrdres() {
        int countBio = egiszOrdersFacade.updateBioOrders();
        eventlogFacade.reserchesLoaded(countBio);
    }

    @Schedule(minute = "*/4", hour = "*", persistent = false)
    public void syncWithoutOrder() {
        fHIRCLient.syncNotPreBoundles();
    }

    @Schedule(minute = "*/3", hour = "*", persistent = false)
    public void syncResult() {
        fHIRCLient.syncOrderResult();
    }        

  
//    @Schedule(minute = "*/3", hour = "*", persistent = false)
//    public void syncOrderRequests() {
//        fHIRCLient.syncOrderRequest();
//    }


    @Schedule(minute = "*/5", hour = "*", persistent = false)
    public void syncIncome() {
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_YEAR, -20);
            List<String> ordersList = fHIRCLient.getOrders(fHIRparameters.getGuid(), c.getTime());
            int count = bundleIncomeLoader.syncIncomeOrder(ordersList);
            eventlogFacade.createLogMessage("Успешный импорт входящих заявок " + count, false);
            for (EgiszTargets x : egiszTargetsFacade.loadByOID(fHIRparameters.getOid())) {
                try {
                    ordersList = fHIRCLient.getOrders(x.getCode(), c.getTime());
                    count = bundleIncomeLoader.syncIncomeOrder(ordersList);
                    eventlogFacade.createLogMessage("Успешный импорт входящих заявок " + count, false);
                } catch (WrongNameException ex) {
                    Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (WrongNameException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
