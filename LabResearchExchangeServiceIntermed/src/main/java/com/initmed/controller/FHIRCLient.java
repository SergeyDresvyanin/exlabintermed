/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.BaseResource;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Condition;
import ca.uhn.fhir.model.dstu2.resource.DiagnosticOrder;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.OperationOutcome;
import ca.uhn.fhir.model.dstu2.resource.Order;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Specimen;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import com.initmed.db.model.DocAnalysOrderAim;
import com.initmed.db.model.EgiszOrderitems;
import com.initmed.facades.EgiszOrdersFacade;
import com.initmed.db.model.EgiszOrders;
import com.initmed.db.model.People;
import com.initmed.db.model.Persons;
import com.initmed.db.model.VexlabOutocomeOrders;
import com.initmed.exceptions.NotFoundException;
import com.initmed.exceptions.WrongDateIntervalException;
import com.initmed.exceptions.WrongDiagnException;
import com.initmed.exceptions.WrongNameException;
import com.initmed.facades.DocAnalysOrderAimFacade;
import com.initmed.facades.EgiszTargetsFacade;
import com.initmed.facades.EventlogFacade;
import com.initmed.facades.PersonsFacade;
import com.initmed.facades.VexlabOutocomeOrdersFacade;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author User
 */
@Stateless
public class FHIRCLient {

    private IGenericClient client;
    private FhirContext ctx;
    @EJB
    private EgiszOrdersFacade egiszOrdersFacade;
    @EJB
    private BundleBioLoader bundleBioLoader;
    @EJB
    private BundleOutcomeLoader bundleOutcomeLoader;
    @EJB
    private FHIRparameters fHIRparameters;
    @EJB
    private EventlogFacade eventlogFacade;
    @EJB
    private VexlabOutocomeOrdersFacade vexlabOutocomeOrdersFacade;
    @EJB
    private PersonsFacade peopleFacade;
    @EJB
    private DocAnalysOrderAimFacade docAnalysOrderAimFacade;
    @EJB
    private EgiszTargetsFacade egiszTargetsFacade;

    @PostConstruct
    void init() {
        this.ctx = FhirContext.forDstu2();
        RequestInterceptor authInterceptor = new RequestInterceptor(fHIRparameters.getApiUrl());
        this.client = ctx.newRestfulGenericClient(fHIRparameters.getApiUrl());
        LoggingInterceptor loggingInterceptor = new LoggingInterceptor();
        this.client.registerInterceptor(loggingInterceptor);
        this.client.registerInterceptor(authInterceptor);
    }

    public void syncNotPreBoundles() {
        try {
            int ordersCount = 0;
            for (EgiszOrders x : egiszOrdersFacade.getNotSendedBoundle()) {
                String msg = null;
                try {
                    sendOrder(x);
                    ordersCount++;
                    for (DocAnalysOrderAim item : x.getDocAnalysOrder().getDocAnalysOrderAims()) {
                        item.setStatusID((short) 6);
                        docAnalysOrderAimFacade.edit(item);
                    }
                    eventlogFacade.createLogMessage("Запись EgiszOrders с id " + x.getId() + " успешно экспортирована", false);
                } catch (Exception e) {
                    msg = e.getMessage();
                    if (msg != null) {
                        if (msg.length() > 799) {
                            msg = msg.substring(0, 750);
                        }
                        x.setErrormsg(msg);
                        egiszOrdersFacade.edit(x);
                        eventlogFacade.createLogMessage("При выгрузке записи с id " + x.getId() + " произошла ошибка" + msg, true);
                    }
                }
            }
            eventlogFacade.reserchesNotPreOrderCreated(ordersCount);
        } catch (Throwable ex) {
            java.util.logging.Logger.getLogger(FHIRCLient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void syncOrderResult() {
        try {
            int ordersCount = 0;
            for (EgiszOrders x : egiszOrdersFacade.getReadyResults()) {
                try {
                    sendOrder(x);
                    ordersCount++;
                    eventlogFacade.createLogMessage("Запись EgiszOrders с id " + x.getId() + " успешно экспортирована ", false);
                } catch (WrongNameException | java.text.ParseException e) {
                    x.setErrormsg(e.getMessage());
                    eventlogFacade.createLogMessage("При выгрузке записи с id " + x.getId() + " произошла ошибка " + e.getMessage(), true);
                }
            }
            eventlogFacade.reserchesNotPreOrderCreated(ordersCount);
        } catch (Throwable ex) {
            java.util.logging.Logger.getLogger(FHIRCLient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void syncOrderRequest() {
        try {
            int ordersCount = 0;
            for (EgiszOrders x : egiszOrdersFacade.getAllNotSentBoundles()) {
                try {
                    sendOrder(x);
                    ordersCount++;
                    eventlogFacade.createLogMessage("Запись результата с id " + x.getId() + " успешно экспортирована", false);
                } catch (WrongNameException | java.text.ParseException e) {
                    eventlogFacade.createLogMessage("При выгрузке результата с id " + x.getId() + " произошла ошибка" + e.getMessage(), true);
                }
                eventlogFacade.reserchesOrder(ordersCount);
            }
        } catch (Throwable ex) {
            java.util.logging.Logger.getLogger(FHIRCLient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String sendOrder(EgiszOrders eo) throws WrongNameException, java.text.ParseException {
        Bundle converted = null;
        if (eo.getIncome() == (short) 1) {
            converted = bundleBioLoader.convertInResultBundle(eo);
            for (EgiszOrderitems x : eo.getEgiszOrderitemsSet()) {
                DocAnalysOrderAim da = x.getDocAnalysOrderAim();
                da.setStatusID((short) 5);
                docAnalysOrderAimFacade.edit(da);
            }
        } else {
            converted = bundleBioLoader.convertInBundleNoPreBloodOrder(eo, false);
        }
        try {
          //  System.out.println(converted);
            Bundle res = createBundle(converted);
            eo.setGuid(res.getId().getIdPart());
            String orderId = getOrderIdFromBundle(res);
            if (orderId != null) {
                eo.setOrederUUID(orderId);
            }
            orderId = getOrderResponseIdFromBundle(res);
            if (orderId != null) {
                eo.setOrederResponseID(orderId);
            }
            eo.setErrormsg(null);
            eo.setTransferTime(new Date());
            egiszOrdersFacade.edit(eo);
            String tmp = FhirContext.forDstu2().newJsonParser().encodeResourceToString(res);
            return tmp;
        } catch (Exception ex) {
            eo.setErrormsg(ex.getMessage());
            eo.setTransferTime(new Date());
            egiszOrdersFacade.edit(eo);
            eventlogFacade.createLogMessage("При выгрузе записи с id " + eo.getId() + " произошла ошибка" + ex.getMessage(), true);
            return "При выгрузе записи с id " + eo.getId() + " произошла ошибка" + ex.getMessage();
        }
    }

    private String getOrderIdFromBundle(Bundle b) {
        List<Bundle.Entry> e = b.getEntry();
        for (Bundle.Entry entry : b.getEntry()) {
            if (entry.getResource().getResourceName().equals("Order")) {
                String res = entry.getResource().getId().getValue();
                return res.replaceAll("Order/", "");
            }
        }
        return null;
    }

    private String getOrderResponseIdFromBundle(Bundle b) {
        List<Bundle.Entry> e = b.getEntry();
        for (Bundle.Entry entry : b.getEntry()) {
            if (entry.getResource().getResourceName().equals("OrderResponse")) {
                String res = entry.getResource().getId().getValue();
                return res.replaceAll("OrderResponse/", "");
            }
        }
        return null;
    }

    public Bundle createBundle(Bundle b) throws IOException, Exception {
        String tmp = FhirContext.forDstu2().newJsonParser().encodeResourceToString(b);
        tmp = tmp.replaceAll("123123", "");
        OkHttpClient okhttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();

        MediaType meta = MediaType.parse("application/json; charset=utf-8");

        //Убрать, добавить вывод лога в БД
       // eventlogFacade.createLogMessage("Запрос: " + tmp, false);
        RequestBody body = RequestBody.create(meta, tmp);
        Request request = new Request.Builder()
                .url(fHIRparameters.getApiUrl())
                .post(body)
                .addHeader("Authorization", "N3 " + fHIRparameters.getToken())
                .build();
        Response response = okhttpClient.newCall(request).execute();
        //Убрать, добавить вывод лога в БД
        //eventlogFacade.createLogMessage("Ответ: " + response.body().string(), false);        
        IParser ip = FhirContext.forDstu2().newJsonParser();
        String res = response.body().string();

        if (response.isSuccessful()) {
            Bundle resBundle = ip.parseResource(Bundle.class, res);
            return resBundle;
        } else {
            StringBuilder sb = new StringBuilder();
            List<OperationOutcome.Issue> list = ip.parseResource(OperationOutcome.class, res).getIssue();
            for (OperationOutcome.Issue x : list) {
                sb.append(x.getDiagnostics()).append(":").append(x.getLocation().toString());
            }
            throw new Exception(sb.toString());
        }
//        Bundle resp = client.transaction().withBundle(b).encodedJson().prettyPrint().execute();
//        return resp.getId().getIdPart();
    }

    public String put(BaseResource br) {
        MethodOutcome outcome = client
                .create()
                .resource(br)
                .encodedJson()
                .execute();
        IdDt id = (IdDt) outcome.getId();
        System.out.println(id.toString());
        return id.getIdPart();
    }

    public String updatePatient(Patient patient, String patoid) {
        patient.setId(patoid);
        MethodOutcome outcome = client.update()
                .resource(patient)
                .execute();
        return ((IdDt) outcome.getId()).getValue();
    }

//    public Bundle.Entry getOrderResponse(String orderMisId, String sourceMo, String targetMO) {
//        Bundle response = client.search()
//                .forResource(OrderResponse.class)
//                .where(OrderResponse.)
//                .and(Patient.ADDRESS.matches().values("Ontario"))
//                .and(Patient.ADDRESS.matches().values("Canada"))
//                .returnBundle(Bundle.class)
//                .execute();
//        return response.getEntryFirstRep();
//    }
    public String getStatus(String UUID) throws IOException, ParseException, Exception {
        OkHttpClient okhttpClient = new OkHttpClient();
        MediaType meta = MediaType.parse("application/json; charset=utf-8");
        JSONObject res = new JSONObject();
        JSONArray params = new JSONArray();
        JSONObject id = new JSONObject();
        id.put("valueString", UUID);
        id.put("name", "OrderId");
        params.add(id);
        res.put("parameter", params);
        res.put("resourceType", "Parameters");

        String jsonRequest = res.toJSONString();

        RequestBody body = RequestBody.create(meta, jsonRequest);

        Request request = new Request.Builder()
                .url(fHIRparameters.getApiUrl() + "/$getstatus")
                .post(body)
                .addHeader("authorization", "N3 " + fHIRparameters.getToken())
                .build();
        Response response = okhttpClient.newCall(request).execute();

        try {
            JSONParser parser = new JSONParser();
            String tmp = response.body().string();
            JSONObject order = (JSONObject) parser.parse(tmp);
            JSONArray jsr = (JSONArray) order.get("parameter");
            return (String) ((JSONObject) jsr.get(0)).get("valueString");
        } catch (ClassCastException | ArrayIndexOutOfBoundsException | NullPointerException e) {
            Logger.getLogger(AdminRest.class.getName()).log(Level.SEVERE, null, e);
            throw new NotFoundException("order not found");
        }
    }

    public Order getOrder(String barcode, String sourceid, String targetId) throws IOException, Exception, Exception {
        OkHttpClient okhttpClient = new OkHttpClient();
        MediaType meta = MediaType.parse("application/json; charset=utf-8");
        JSONObject reqJSON = new JSONObject();
        JSONArray params = new JSONArray();
        JSONObject barcodeObject = new JSONObject();
        barcodeObject.put("valueString", barcode);
        barcodeObject.put("name", "Barcode");
        JSONObject targetObject = new JSONObject();
        targetObject.put("valueString", targetId);
        targetObject.put("name", "TargetCode");
        JSONObject sourceObject = new JSONObject();
        sourceObject.put("valueString", sourceid);
        sourceObject.put("name", "SourceCode");
        params.add(sourceObject);
        params.add(targetObject);
        params.add(barcodeObject);
        reqJSON.put("parameter", params);
        reqJSON.put("resourceType", "Parameters");
        String jsonRequest = reqJSON.toJSONString();
        RequestBody body = RequestBody.create(meta, jsonRequest);
        Request request = new Request.Builder()
                .url(fHIRparameters.getApiUrl() + "/$getorder")
                .post(body)
                .addHeader("authorization", "N3 " + fHIRparameters.getToken())
                .build();
        Response response = okhttpClient.newCall(request).execute();
        IParser ip = FhirContext.forDstu2().newJsonParser();
        try {
            JSONParser parser = new JSONParser();
            JSONObject order = (JSONObject) parser.parse(response.body().string());
            JSONArray jsr = (JSONArray) order.get("parameter");
            String res = (String) ((JSONObject) ((JSONObject) jsr.get(0)).get("resource")).get("id");

            if (response.isSuccessful()) {
                Order resBundle = ip.parseResource(Order.class, res);
                return resBundle;
            }
            throw new NotFoundException(response.body().string());
        } catch (ClassCastException | ArrayIndexOutOfBoundsException | NullPointerException e) {
            throw new NotFoundException("order not found");
        }
    }

    public Order getOrder(String refrence) throws Exception {
        IParser ip = FhirContext.forDstu2().newJsonParser();
        String r = getResource(refrence);
        Order resOrder = ip.parseResource(Order.class, r);
        return resOrder;
    }

    public Patient getPatient(String refrence) throws Exception {
        IParser ip = FhirContext.forDstu2().newJsonParser();
        String r = getResource(refrence);
        Patient resPat = ip.parseResource(Patient.class, r);
        return resPat;
    }

    public DiagnosticOrder getDiagnosticOrder(String refrence) throws Exception {
        IParser ip = FhirContext.forDstu2().newJsonParser();
        String r = getResource(refrence);
 //       System.out.println(r);
        DiagnosticOrder resDOrder = ip.parseResource(DiagnosticOrder.class, r);
        return resDOrder;
    }

    public List<DiagnosticOrder> getDiagnosticOrder(List<ResourceReferenceDt> refrenceList) throws Exception {
        List<DiagnosticOrder> dOrders = new ArrayList<>();
        for (ResourceReferenceDt x : refrenceList) {
            DiagnosticOrder d = getDiagnosticOrder(x.getReference().getValue());
            if (d != null) {
                dOrders.add(d);
            }
        }
        return dOrders;
    }

    public Specimen getSpecimen(String refrence) throws Exception {
        IParser ip = FhirContext.forDstu2().newJsonParser();
        String r = getResource(refrence);
 //       System.out.println(r);
        Specimen resSpice = ip.parseResource(Specimen.class, r);
        return resSpice;
    }

    public Encounter getEncounter(String refrence) throws Exception {
        IParser ip = FhirContext.forDstu2().newJsonParser();
        String r = getResource(refrence);
        Encounter resEnc = ip.parseResource(Encounter.class, r);
        return resEnc;
    }

    public Condition getCondition(String refrence) throws Exception {
        IParser ip = FhirContext.forDstu2().newJsonParser();
        String r = getResource(refrence);
        Condition resCond = ip.parseResource(Condition.class, r);
        return resCond;
    }

    public String getResource(String refrence) throws IOException, Exception, Exception {
        OkHttpClient okhttpClient = new OkHttpClient();

        Request request = new Request.Builder()
                .url(fHIRparameters.getApiUrl() + "/" + refrence)
                .get()
                .addHeader("authorization", "N3 " + fHIRparameters.getToken())
                .build();
        Response response = okhttpClient.newCall(request).execute();
        try {
            if (response.isSuccessful()) {
                return response.body().string();
            }
            throw new NotFoundException(response.body().string());
        } catch (ClassCastException | ArrayIndexOutOfBoundsException | NullPointerException e) {
            throw new NotFoundException("order not found");
        }
    }

    public List<String> getOrders(String labId, Date startDate) throws IOException, Exception, Exception {
        List<String> res = new ArrayList<>();
        OkHttpClient okhttpClient = new OkHttpClient();
        MediaType meta = MediaType.parse("application/json; charset=utf-8");
        JSONObject reqJSON = new JSONObject();
        JSONArray params = new JSONArray();
        JSONObject target = new JSONObject();
        target.put("valueString", labId);
        target.put("name", "TargetCode");
        JSONObject datObj = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        datObj.put("valueString", sdf.format(startDate));
        datObj.put("name", "StartDate");

        JSONObject sorObj = new JSONObject();
        sorObj.put("valueString", "Date");
        sorObj.put("name", "_sort:desc");

        params.add(datObj);
        params.add(target);
        params.add(sorObj);
        reqJSON.put("parameter", params);
        reqJSON.put("resourceType", "Parameters");
        String jsonRequest = reqJSON.toJSONString();
        RequestBody body = RequestBody.create(meta, jsonRequest);
        Request request = new Request.Builder()
                .url(fHIRparameters.getApiUrl() + "/$getorders")
                .post(body)
                .addHeader("authorization", "N3 " + fHIRparameters.getToken())
                .build();
        Response response = okhttpClient.newCall(request).execute();
        try {
            if (response.isSuccessful()) {
                JSONParser parser = new JSONParser();
                String s = response.body().string();
 //               System.out.println(s);
                JSONObject order = (JSONObject) parser.parse(s);
                JSONArray jsr = (JSONArray) order.get("parameter");
                if (jsr != null) {
                    for (Object orderObj : jsr) {
                        String ordId = ((JSONObject) ((JSONObject) orderObj).get("resource")).get("id").toString();
                        res.add(ordId);
                    }
                }
            }
        } catch (ClassCastException | ArrayIndexOutOfBoundsException | NullPointerException e) {
            throw new NotFoundException(e.getMessage());
        }
        return res;
    }

    public String validateRequest(String diag, Long peopleId, Integer aimId) throws Exception {
        Persons people = peopleFacade.find(peopleId);
        if (people == null) {
            return "Пациент не найден";
        }
        Patient p = createPatient(bundleOutcomeLoader.convertPersonsToPatient(people));
        return validateRequest(diag, p.getId().toString(), egiszOrdersFacade.getResearch(aimId));
    }

    public String validateRequest(String diag, String patGUID, String research) throws Exception {
        OkHttpClient okhttpClient = new OkHttpClient();
        MediaType meta = MediaType.parse("application/json; charset=utf-8");
        JSONObject reqJSON = new JSONObject();
        JSONArray params = new JSONArray();
        JSONObject barcodeObject = new JSONObject();
        barcodeObject.put("valueString", research);
        barcodeObject.put("name", "Code");
        JSONObject targetObject = new JSONObject();
        targetObject.put("valueString", patGUID);
        targetObject.put("name", "Patient");
//        JSONObject sourceObject = new JSONObject();
//        sourceObject.put("valueString", diag);
//        sourceObject.put("name", "Diagnosis");
//        params.add(sourceObject);
        params.add(targetObject);
        params.add(barcodeObject);
        reqJSON.put("parameter", params);
        reqJSON.put("resourceType", "Parameters");
        String jsonRequest = reqJSON.toJSONString();
        RequestBody body = RequestBody.create(meta, jsonRequest);
        Request request = new Request.Builder()
                .url(fHIRparameters.getApiUrl() + "/$validity")
                .post(body)
                .addHeader("authorization", "N3 " + fHIRparameters.getToken())
                .build();
        Response response = okhttpClient.newCall(request).execute();
        JSONParser parser = new JSONParser();
        String str = response.body().string();
        JSONObject order = (JSONObject) parser.parse(str);
 //       System.out.println(str);
        if (response.code() == 200 || response.code() == 201) {
            JSONArray jsr = (JSONArray) order.get("parameter");
            Iterator resIter = jsr.iterator();
            String res = "";
            while (resIter.hasNext()) {

                JSONObject x = (JSONObject) resIter.next();
                if (x.get("name").equals("Diagnosis") && x.get("valueBoolean").equals("false")) {
                    res = res + " " + "назначение признано необоснованным в связи с диагнозом";
                }
                if (x.get("name").equals("Date") && x.get("valueBoolean").equals("false")) {
                    res = res + " " + "необоснованная дата направления";
                }
            }
            return res;

        } else {
            JSONArray jsr = (JSONArray) order.get("issue");
            return (String) ((JSONObject) jsr.get(0)).get("diagnostics");
        }
    }

    public Patient createPatient(Patient p) throws IOException, Exception {
        String tmp = FhirContext.forDstu2().newJsonParser().encodeResourceToString(p);
        OkHttpClient okhttpClient = new OkHttpClient();
        MediaType meta = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(meta, tmp);
        Request request = new Request.Builder()
                .url(fHIRparameters.getApiUrl() + "/Patient")
                .post(body)
                .addHeader("Authorization", "N3 " + fHIRparameters.getToken())
                .build();
        Response response = okhttpClient.newCall(request).execute();
        IParser ip = FhirContext.forDstu2().newJsonParser();
        if (response.isSuccessful()) {
            Patient resBundle = ip.parseResource(Patient.class, response.body().string());
            return resBundle;
        } else {
            StringBuilder sb = new StringBuilder();
            List<OperationOutcome.Issue> list = ip.parseResource(OperationOutcome.class, response.body().string()).getIssue();
            for (OperationOutcome.Issue x : list) {
                sb.append(x.getDiagnostics()).append(":").append(x.getLocation().toString());
            }
            throw new Exception(sb.toString());
        }
//        Bundle resp = client.transaction().withBundle(b).encodedJson().prettyPrint().execute();
//        return resp.getId().getIdPart();
    }
}
