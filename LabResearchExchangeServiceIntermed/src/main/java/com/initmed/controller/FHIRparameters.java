/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import com.initmed.facades.ParametersLoader;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author sd199
 */
@Startup
@Singleton
public class FHIRparameters {

    @EJB
    private ParametersLoader parametersLoader;

    private String oid;
    private String guid;
    private String token;
    private String apiUrl;
    private String name;
    private String addr;
    private String tel;
    private String region;
    private String ogrn;
    private String misOid;

    @PostConstruct
    public void init() {
        if (this.oid == null) {
            this.oid = parametersLoader.loadParameterByName("LABOID");
            this.guid = parametersLoader.loadParameterByName("LABORGOGUID");
            this.token = parametersLoader.loadParameterByName("FHIR_MISGUID");
            this.apiUrl = parametersLoader.loadParameterByName("FHIR_URL");
            this.name = parametersLoader.loadParameterByName("EMKORGNAME");
            this.addr = parametersLoader.loadParameterByName("EMKADDR");
            this.tel = parametersLoader.loadParameterByName("EMKTEL");
            this.misOid = parametersLoader.loadParameterByName("FHIR_MISOID");
            this.region = parametersLoader.loadParameterByName("REGION");
            this.ogrn = parametersLoader.loadParameterByName("OGRN");
        }
    }

    public String getOid() {
        return oid;
    }

    public String getGuid() {
        return guid;
    }

    public ParametersLoader getParametersLoader() {
        return parametersLoader;
    }

    public String getToken() {
        return token;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getName() {
        return name;
    }

    public String getAddr() {
        return addr;
    }

    public String getTel() {
        return tel;
    }

    public String getRegion() {
        return region;
    }

    public String getOgrn() {
        return ogrn;
    }

    public String getMisOid() {
        return misOid;
    }

}
