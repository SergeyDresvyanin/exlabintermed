/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller;

import ca.uhn.fhir.rest.client.api.IClientInterceptor;
import ca.uhn.fhir.rest.client.api.IHttpRequest;
import ca.uhn.fhir.rest.client.api.IHttpResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class RequestInterceptor implements IClientInterceptor {

    private String apiKey;

    public RequestInterceptor(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public void interceptRequest(IHttpRequest ihr) {
        ihr.addHeader("authorization", "N3 " + apiKey);
        try {
            if (ihr.getRequestBodyFromStream() != null) {
                ihr.getRequestBodyFromStream().replaceAll("123123", "");
            }
           // Logger.getLogger(RequestInterceptor.class.getName()).log(Level.SEVERE, null, ihr.getUri());
            Map<String, List<String>> headers = ihr.getAllHeaders();
            for (Entry<String, List<String>> x : headers.entrySet()) {
                String res = x.getKey() + ":";
                for (String y : x.getValue()) {
                    res = res + " " + y;
                }
                Logger.getLogger(RequestInterceptor.class.getName()).log(Level.SEVERE, null, res);
            }
           // Logger.getLogger(RequestInterceptor.class.getName()).log(Level.SEVERE, null, ihr.getRequestBodyFromStream());
        } catch (IOException ex) {
            Logger.getLogger(RequestInterceptor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void interceptResponse(IHttpResponse ihr) throws IOException {

    }

}
