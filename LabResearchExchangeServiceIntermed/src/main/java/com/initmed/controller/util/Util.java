/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.controller.util;

import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.PeriodDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.primitive.StringDt;
import com.initmed.db.model.Persons;
import com.initmed.exceptions.WrongNameException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author User
 */
public class Util {

    public static IdentifierDt createIdt(String sys, String value, String display, Date start, Date end) {
        IdentifierDt er = new IdentifierDt();
        er.setSystem(sys);
        er.setValue(value);
        if (start != null) {
            PeriodDt pdt = new PeriodDt();
            pdt.setStart(new DateTimeDt(start));
            pdt.setEnd(new DateTimeDt(end));
            er.setPeriod(pdt);
        }
        ResourceReferenceDt rd = new ResourceReferenceDt();
        rd.setDisplay(display);
        er.setAssigner(rd);
        return er;
    }

    public static IdentifierDt createIdt(String sys, String value, String display) {
        return createIdt(sys, value, display, null, null);
    }

    public static CodeableConceptDt createCoding(String oid, String version, String code) {
        CodeableConceptDt cdt = new CodeableConceptDt();
        CodingDt c = new CodingDt();
        c.setSystem(oid);
        c.setVersion(version);
        c.setCode(code);
        cdt.addCoding(c);
        return cdt;
    }

    public static List<HumanNameDt> getHumanName(Persons persons) throws WrongNameException {
        HumanNameDt res = new HumanNameDt();
        List<StringDt> famList = new ArrayList<>();
        List<StringDt> givenList = new ArrayList<>();
        StringDt family = new StringDt(persons.getLastName());
        StringDt name = new StringDt(persons.getFirstName());
        StringDt patronomic = new StringDt(persons.getMiddleName());

//        StringDt family = new StringDt("Тестов");
//        StringDt name = new StringDt("Тест");
//        StringDt patronomic = new StringDt("Тестович");
        famList.add(family);
        famList.add(patronomic);
        givenList.add(name);
        res.setFamily(famList);
        res.setGiven(givenList);
        List<HumanNameDt> hndtList = new ArrayList<>();
        hndtList.add(res);
        return hndtList;
    }

    public static List<HumanNameDt> getDocHumanName(Persons persons) throws WrongNameException {
        HumanNameDt res = new HumanNameDt();
        List<StringDt> famList = new ArrayList<>();
        List<StringDt> givenList = new ArrayList<>();
        StringDt family = new StringDt(persons.getLastName());
        StringDt name = new StringDt(persons.getFirstName() + " " + persons.getMiddleName());
        StringDt patronomic = new StringDt(persons.getMiddleName());
//
//        StringDt family = new StringDt("Ковеленов");
//        StringDt name = new StringDt("Алексей");
//        StringDt patronomic = new StringDt("Юрьевич");
        famList.add(family);
//        famList.add(patronomic);
        givenList.add(name);
        res.setFamily(famList);
        res.setGiven(givenList);
        List<HumanNameDt> hndtList = new ArrayList<>();
        hndtList.add(res);
        return hndtList;
    }

    public static List<HumanNameDt> getHumanName(String fullname) throws WrongNameException {
        HumanNameDt res = new HumanNameDt();
        String[] fio = fullname.split("\\s");
        List<StringDt> givenList = new ArrayList<>();
        if (fio.length == 3) {

//            StringDt family = new StringDt("Ковеленов");
//            StringDt name = new StringDt("Алексей");
//            StringDt patronomic = new StringDt("Юрьевич");
            StringDt family = new StringDt(fio[0]);
            StringDt name = new StringDt(fio[1]);
            StringDt patronomic = new StringDt(fio[2]);
            givenList.add(name);
            res.addFamily(family);
            res.addFamily(patronomic);
            res.setGiven(givenList);
            List<HumanNameDt> hndtList = new ArrayList<>();
            hndtList.add(res);
            return hndtList;
        } else {
            throw new WrongNameException(fullname);
        }
    }

}
