/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "DICT_DOCTORS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DictDoctors.findAll", query = "SELECT d FROM DictDoctors d"),
    @NamedQuery(name = "DictDoctors.findById", query = "SELECT d FROM DictDoctors d WHERE d.id = :id")})
public class DictDoctors implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 500)
    @Column(name = "FIO")
    private String fio;
    @Column(name = "FOMS_POSITION")
    private Integer fomsPosition;
    @Column(name = "SNILS")
    private String snils;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VISIBLE")
    private boolean visible;
    @Size(max = 100)
    @Column(name = "LAST_NAME")
    private String lastName;
    @Size(max = 100)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 100)
    @Column(name = "MIDDLE_NAME")
    private String middleName;
    @Column(name = "EMPLOYEE_CLASS")
    private Integer employeeClass;
    @Size(max = 2147483647)
    @Column(name = "POSITIONS_TEXT")
    private String positionsText;
    @Size(max = 2147483647)
    @Column(name = "SPECIALITIES_TEXT")
    private String specialitiesText;

    public DictDoctors() {
    }

    public DictDoctors(Integer id) {
        this.id = id;
    }

    public DictDoctors(Integer id, boolean visible) {
        this.id = id;
        this.visible = visible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Integer getFomsPosition() {
        return fomsPosition;
    }

    public void setFomsPosition(Integer fomsPosition) {
        this.fomsPosition = fomsPosition;
    }

    public boolean getVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Integer getEmployeeClass() {
        return employeeClass;
    }

    public void setEmployeeClass(Integer employeeClass) {
        this.employeeClass = employeeClass;
    }

    public String getPositionsText() {
        return positionsText;
    }

    public void setPositionsText(String positionsText) {
        this.positionsText = positionsText;
    }

    public String getSpecialitiesText() {
        return specialitiesText;
    }

    public void setSpecialitiesText(String specialitiesText) {
        this.specialitiesText = specialitiesText;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DictDoctors)) {
            return false;
        }
        DictDoctors other = (DictDoctors) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.model.DictDoctors[ id=" + id + " ]";
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

}
