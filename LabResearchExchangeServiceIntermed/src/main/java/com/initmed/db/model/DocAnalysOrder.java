/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "DOCTOR_ANALYS_ORDER")
@XmlRootElement
public class DocAnalysOrder {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "ORDER_NUM")
    private String orderNum;
    @Column(name = "DATE")
    private Date date;
    @Column(name = "DOCTOR_ID")
    private Integer docId;
    @JoinColumn(name = "PEOPLE_ID", referencedColumnName = "PEOPLE_ID")
    @ManyToOne
    private People people;
    @Column(name = "PAYED")
    private Short payed;
    @Column(name = "SUM")
    private Double sum;
    @Column(name = "TRANSFERED")
    private Short transfered;
    @Column(name = "ORDER_AIM")
    private Integer ordeerAim;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refAnalysOrder")
    private Set<DocAnalysOrderAim> docAnalysOrderAims;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "docAnalysOrder")
    private Set<EgiszOrders> egiszOrderses;

    public DocAnalysOrder() {
    }

    public DocAnalysOrder(String orderNum, Date date, Integer docId, Short payed, Double sum, Short transfered, Integer ordeerAim, Set<DocAnalysOrderAim> docAnalysOrderAims) {
        this.orderNum = orderNum;
        this.date = date;
        this.docId = docId;

        this.payed = payed;
        this.sum = sum;
        this.transfered = transfered;
        this.ordeerAim = ordeerAim;
        this.docAnalysOrderAims = docAnalysOrderAims;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public Short getPayed() {
        return payed;
    }

    public void setPayed(Short payed) {
        this.payed = payed;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Short getTransfered() {
        return transfered;
    }

    public void setTransfered(Short transfered) {
        this.transfered = transfered;
    }

    public Integer getOrdeerAim() {
        return ordeerAim;
    }

    public void setOrdeerAim(Integer ordeerAim) {
        this.ordeerAim = ordeerAim;
    }

    public Set<DocAnalysOrderAim> getDocAnalysOrderAims() {
        return docAnalysOrderAims;
    }

    public void setDocAnalysOrderAims(Set<DocAnalysOrderAim> docAnalysOrderAims) {
        this.docAnalysOrderAims = docAnalysOrderAims;
    }

    public Set<EgiszOrders> getEgiszOrderses() {
        return egiszOrderses;
    }

    public void setEgiszOrderses(Set<EgiszOrders> egiszOrderses) {
        this.egiszOrderses = egiszOrderses;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

}
