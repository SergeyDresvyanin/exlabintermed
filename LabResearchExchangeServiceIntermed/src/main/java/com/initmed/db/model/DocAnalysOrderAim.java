/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "DOCTOR_ANALYS_ORDER_AIMS")
@XmlRootElement
public class DocAnalysOrderAim {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "ANALYS_AIM")
    private Integer analysAim;
    @Column(name = "MATERIAL")
    private Integer material;
    @Column(name = "TRANSFERED")
    private Short transfered;
    @Column(name = "StatusID")
    private Short statusID;

    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private DocAnalysOrder refAnalysOrder;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "docAnalysOrderAim")
    private Set<EgiszOrderitems> dEgiszOrderitemses;

    public DocAnalysOrderAim() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DocAnalysOrder getRefAnalysOrder() {
        return refAnalysOrder;
    }

    public void setRefAnalysOrder(DocAnalysOrder refAnalysOrder) {
        this.refAnalysOrder = refAnalysOrder;
    }

    public Integer getAnalysAim() {
        return analysAim;
    }

    public void setAnalysAim(Integer analysAim) {
        this.analysAim = analysAim;
    }

    public Integer getMaterial() {
        return material;
    }

    public void setMaterial(Integer material) {
        this.material = material;
    }

    public Short getTransfered() {
        return transfered;
    }

    public void setTransfered(Short transfered) {
        this.transfered = transfered;
    }

    public Short getStatusID() {
        return statusID;
    }

    public void setStatusID(Short statusID) {
        this.statusID = statusID;
    }

    public Set<EgiszOrderitems> getdEgiszOrderitemses() {
        return dEgiszOrderitemses;
    }

    public void setdEgiszOrderitemses(Set<EgiszOrderitems> dEgiszOrderitemses) {
        this.dEgiszOrderitemses = dEgiszOrderitemses;
    }

}
