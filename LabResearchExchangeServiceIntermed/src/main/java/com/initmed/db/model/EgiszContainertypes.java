/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "EGISZ_EXLAB_CONTAINERTYPES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EgiszContainertypes.findAll", query = "SELECT e FROM EgiszContainertypes e"),
    @NamedQuery(name = "EgiszContainertypes.findById", query = "SELECT e FROM EgiszContainertypes e WHERE e.id = :id"),
    @NamedQuery(name = "EgiszContainertypes.findByName", query = "SELECT e FROM EgiszContainertypes e WHERE e.name = :name"),
    @NamedQuery(name = "EgiszContainertypes.findByIsvisible", query = "SELECT e FROM EgiszContainertypes e WHERE e.isvisible = :isvisible")})
public class EgiszContainertypes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISVISIBLE")
    private short isvisible;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refegiszContainertypes")
    private Set<EgiszOrderitems> egiszOrdersSet;

    public EgiszContainertypes() {
    }

    public EgiszContainertypes(Integer id) {
        this.id = id;
    }

    public EgiszContainertypes(Integer id, String name, short isvisible) {
        this.id = id;
        this.name = name;
        this.isvisible = isvisible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(short isvisible) {
        this.isvisible = isvisible;
    }

    public Set<EgiszOrderitems> getEgiszOrdersSet() {
        return egiszOrdersSet;
    }

    public void setEgiszOrdersSet(Set<EgiszOrderitems> egiszOrdersSet) {
        this.egiszOrdersSet = egiszOrdersSet;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszContainertypes)) {
            return false;
        }
        EgiszContainertypes other = (EgiszContainertypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.EgiszContainertypes[ id=" + id + " ]";
    }

}
