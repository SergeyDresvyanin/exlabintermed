/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "EGISZ_EXLAB_MEASUREMENTTYPE")
@XmlRootElement
public class EgiszMeasureTypes {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refMeasureType")
    private Set<VExlabVLabTestResult> refMeasureType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<VExlabVLabTestResult> getRefMeasureType() {
        return refMeasureType;
    }

    public void setRefMeasureType(Set<VExlabVLabTestResult> refMeasureType) {
        this.refMeasureType = refMeasureType;
    }

}
