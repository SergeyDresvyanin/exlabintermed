/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@Entity
@Table(name = "EGISZ_EXLAB_ORDERITEMS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EgiszOrderitems.findAll", query = "SELECT e FROM EgiszOrderitems e"),
    @NamedQuery(name = "EgiszOrderitems.findById", query = "SELECT e FROM EgiszOrderitems e WHERE e.id = :id")
})
public class EgiszOrderitems implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "REFEGISZ_PRIORITY")
    private String refegiszPriority;
    @JoinColumn(name = "REFEGISZ_ORDERS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private EgiszOrders refegiszOrders;
    @JoinColumn(name = "REFEGISZ_RESEARCHTYPES", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private EgiszResearchtypes refegiszResearchtypes;
    @JoinColumn(name = "REFEGISZRESEARCHGROUP", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private EgiszResearchGroup egiszResearchGroup;
    @JoinColumn(name = "REFEGISZ_CONTAINERTYPES", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private EgiszContainertypes refegiszContainertypes;
    @JoinColumn(name = "REFEGISZ_MATERIALS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private EgiszMaterials refegiszMaterials;
    @Column(name = "DIAGNORDERID")
    private String diagnOrderId;
    @Column(name = "SPECIMENID")
    private String spicimenId;
    @Column(name = "TAKINGDATE")
    @Temporal(TemporalType.DATE)
    private Date takingdate;

    @JoinColumn(name = "REFORDEDRAIM", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private DocAnalysOrderAim docAnalysOrderAim;

    public EgiszOrderitems() {
    }

    public EgiszOrderitems(Integer id) {
        this.id = id;
    }

    public EgiszOrderitems(Integer id, String refegiszPriority) {
        this.id = id;
        this.refegiszPriority = refegiszPriority;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRefegiszPriority() {
        return refegiszPriority;
    }

    public void setRefegiszPriority(String refegiszPriority) {
        this.refegiszPriority = refegiszPriority;
    }

    public EgiszOrders getRefegiszOrders() {
        return refegiszOrders;
    }

    public void setRefegiszOrders(EgiszOrders refegiszOrders) {
        this.refegiszOrders = refegiszOrders;
    }

    public EgiszResearchtypes getRefegiszResearchtypes() {
        return refegiszResearchtypes;
    }

    public void setRefegiszResearchtypes(EgiszResearchtypes refegiszResearchtypes) {
        this.refegiszResearchtypes = refegiszResearchtypes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszOrderitems)) {
            return false;
        }
        EgiszOrderitems other = (EgiszOrderitems) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.EgiszOrderitems[ id=" + id + " ]";
    }

    public EgiszResearchGroup getEgiszResearchGroup() {
        return egiszResearchGroup;
    }

    public void setEgiszResearchGroup(EgiszResearchGroup egiszResearchGroup) {
        this.egiszResearchGroup = egiszResearchGroup;
    }

    public EgiszContainertypes getRefegiszContainertypes() {
        return refegiszContainertypes;
    }

    public void setRefegiszContainertypes(EgiszContainertypes refegiszContainertypes) {
        this.refegiszContainertypes = refegiszContainertypes;
    }

    public EgiszMaterials getRefegiszMaterials() {
        return refegiszMaterials;
    }

    public void setRefegiszMaterials(EgiszMaterials refegiszMaterials) {
        this.refegiszMaterials = refegiszMaterials;
    }

    public String getDiagnOrderId() {
        return diagnOrderId;
    }

    public void setDiagnOrderId(String diagnOrderId) {
        this.diagnOrderId = diagnOrderId;
    }

    public String getSpicimenId() {
        return spicimenId;
    }

    public void setSpicimenId(String spicimenId) {
        this.spicimenId = spicimenId;
    }

    public DocAnalysOrderAim getDocAnalysOrderAim() {
        return docAnalysOrderAim;
    }

    public void setDocAnalysOrderAim(DocAnalysOrderAim docAnalysOrderAim) {
        this.docAnalysOrderAim = docAnalysOrderAim;
    }

    public Date getTakingdate() {
        return takingdate;
    }

    public void setTakingdate(Date takingdate) {
        this.takingdate = takingdate;
    }

}
