/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "EGISZ_EXLAB_ORDERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EgiszOrders.getByOrderUUID", query = "SELECT e FROM EgiszOrders e WHERE e.orederUUID=:guid and e.income=1"),
    @NamedQuery(name = "EgiszOrders.getByDiagOrderUUID", query = "SELECT e FROM EgiszOrders e WHERE e.diagOrderUUID=:guid and e.income=1")})
public class EgiszOrders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "CREATETIME")
    private Date createtime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "HEIGHT")
    private Double height;
    @Column(name = "WEIGHT")
    private Double weight;
    @Column(name = "DIAGNOSISDATE")
    @Temporal(TemporalType.DATE)
    private Date diagnosisdate;
    @Column(name = "MKB")
    private String mkb;
    @Column(name = "SOURCE")
    private String source;
    @Column(name = "DESCR")
    private String descr;
    @Column(name = "GUID")
    private String guid;
    @Column(name = "ERRORMSG")
    private String errormsg;
    @Column(name = "TRANSFERTIME")
    private Date transferTime;
    @Column(name = "REFOUTCOMEORDER")
    private Integer refoutcomeOrder;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refegiszOrders")
    private Set<EgiszOrderitems> egiszOrderitemsSet;
    @Column(name = "INCOME")
    @NotNull
    private Short income;
    @JoinColumn(name = "REFORDER", referencedColumnName = "ID")
    @ManyToOne
    private DocAnalysOrder docAnalysOrder;
    @Column(name = "PROCESSEDINBD")
    private Short processedindb;
    @JoinColumn(name = "REFEGISZ_ORDERSTATUS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private EgiszOrderstatus refegiszOrderstatus;
    @JoinColumn(name = "REFEGISZ_TARGETS", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private EgiszTargets refegiszTargets;
    @JoinColumn(name = "REFTEMPPERSONS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private TempPersons refpersons;
    @Column(name = "ORDERUUID")
    private String orederUUID;
    @Column(name = "ORDERRESPONSEID")
    private String orederResponseID;
    @Column(name="diagn_order_uuid")
    private String diagOrderUUID;

    @Column(name = "APPOINTMENTID")
    private Integer appointmentId;

    public EgiszOrders() {
    }

    public EgiszOrders(Integer id) {
        this.id = id;
    }

    public EgiszOrders(Integer id, Date createtime, Date diagnosisdate) {
        this.id = id;
        this.createtime = createtime;
        this.diagnosisdate = diagnosisdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Date getDiagnosisdate() {
        return diagnosisdate;
    }

    public void setDiagnosisdate(Date diagnosisdate) {
        this.diagnosisdate = diagnosisdate;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    @XmlTransient
    public Set<EgiszOrderitems> getEgiszOrderitemsSet() {
        return egiszOrderitemsSet;
    }

    public void setEgiszOrderitemsSet(Set<EgiszOrderitems> egiszOrderitemsSet) {
        this.egiszOrderitemsSet = egiszOrderitemsSet;
    }

    public EgiszOrderstatus getRefegiszOrderstatus() {
        return refegiszOrderstatus;
    }

    public void setRefegiszOrderstatus(EgiszOrderstatus refegiszOrderstatus) {
        this.refegiszOrderstatus = refegiszOrderstatus;
    }

    public EgiszTargets getRefegiszTargets() {
        return refegiszTargets;
    }

    public void setRefegiszTargets(EgiszTargets refegiszTargets) {
        this.refegiszTargets = refegiszTargets;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszOrders)) {
            return false;
        }
        EgiszOrders other = (EgiszOrders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.EgiszOrders[ id=" + id + " ]";
    }

    public String getMkb() {
        return mkb;
    }

    public void setMkb(String mkb) {
        this.mkb = mkb;
    }

    public Short getIncome() {
        return income;
    }

    public void setIncome(Short income) {
        this.income = income;
    }

    public Short getProcessedindb() {
        return processedindb;
    }

    public void setProcessedindb(Short processedindb) {
        this.processedindb = processedindb;
    }

    public TempPersons getRefpersons() {
        return refpersons;
    }

    public void setRefpersons(TempPersons refpersons) {
        this.refpersons = refpersons;
    }

    public String getOrederUUID() {
        return orederUUID;
    }

    public void setOrederUUID(String orederUUID) {
        this.orederUUID = orederUUID;
    }
    
    public String getOrederResponseID() {
        return orederResponseID;
    }

    public void setOrederResponseID(String orederResponseID) {
        this.orederResponseID = orederResponseID;
    }

    public Integer getRefoutcomeOrder() {
        return refoutcomeOrder;
    }

    public void setRefoutcomeOrder(Integer refoutcomeOrder) {
        this.refoutcomeOrder = refoutcomeOrder;
    }

    public DocAnalysOrder getDocAnalysOrder() {
        return docAnalysOrder;
    }

    public void setDocAnalysOrder(DocAnalysOrder docAnalysOrder) {
        this.docAnalysOrder = docAnalysOrder;
    }

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Date getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(Date transferTime) {
        this.transferTime = transferTime;
    }

    public String getDiagOrderUUID() {
        return diagOrderUUID;
    }

    public void setDiagOrderUUID(String diagOrderUUID) {
        this.diagOrderUUID = diagOrderUUID;
    }

}
