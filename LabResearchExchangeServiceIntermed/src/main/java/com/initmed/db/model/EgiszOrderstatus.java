/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "EGISZ_EXLAB_ORDERSTATUS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EgiszOrderstatus.findAll", query = "SELECT e FROM EgiszOrderstatus e"),
    @NamedQuery(name = "EgiszOrderstatus.findById", query = "SELECT e FROM EgiszOrderstatus e WHERE e.id = :id"),
    @NamedQuery(name = "EgiszOrderstatus.findByCode", query = "SELECT e FROM EgiszOrderstatus e WHERE e.code = :code"),
    @NamedQuery(name = "EgiszOrderstatus.findByName", query = "SELECT e FROM EgiszOrderstatus e WHERE e.name = :name"),
    @NamedQuery(name = "EgiszOrderstatus.findByIsvisible", query = "SELECT e FROM EgiszOrderstatus e WHERE e.isvisible = :isvisible")})
public class EgiszOrderstatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISVISIBLE")
    private short isvisible;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refegiszOrderstatus")
    private Set<EgiszOrders> egiszOrdersSet;

    public EgiszOrderstatus() {
    }

    public EgiszOrderstatus(Integer id) {
        this.id = id;
    }

    public EgiszOrderstatus(Integer id, String code, String name, short isvisible) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.isvisible = isvisible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(short isvisible) {
        this.isvisible = isvisible;
    }

    @XmlTransient
    public Set<EgiszOrders> getEgiszOrdersSet() {
        return egiszOrdersSet;
    }

    public void setEgiszOrdersSet(Set<EgiszOrders> egiszOrdersSet) {
        this.egiszOrdersSet = egiszOrdersSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszOrderstatus)) {
            return false;
        }
        EgiszOrderstatus other = (EgiszOrderstatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.EgiszOrderstatus[ id=" + id + " ]";
    }
    
}
