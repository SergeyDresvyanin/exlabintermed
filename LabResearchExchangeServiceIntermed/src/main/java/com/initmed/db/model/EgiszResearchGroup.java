/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "EGISZ_EXLAB_RESEARCH_GROUP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EgiszResearchGroup.findAll", query = "SELECT e FROM EgiszResearchGroup e"),
    @NamedQuery(name = "EgiszResearchGroup.findById", query = "SELECT e FROM EgiszResearchGroup e WHERE e.id = :id"),
    @NamedQuery(name = "EgiszResearchGroup.findByName", query = "SELECT e FROM EgiszResearchGroup e WHERE e.name = :name"),
    @NamedQuery(name = "EgiszResearchGroup.findByIsvisible", query = "SELECT e FROM EgiszResearchGroup e WHERE e.isvisible = :isvisible")})
public class EgiszResearchGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISVISIBLE")
    private short isvisible;
    @OneToMany(mappedBy = "egiszResearchGroup")
    private Set<EgiszOrderitems> egiszOrdersSet;

    public EgiszResearchGroup() {
    }

    public EgiszResearchGroup(Integer id) {
        this.id = id;
    }

    public EgiszResearchGroup(Integer id, short isvisible) {
        this.id = id;
        this.isvisible = isvisible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(short isvisible) {
        this.isvisible = isvisible;
    }

    public Set<EgiszOrderitems> getEgiszOrdersSet() {
        return egiszOrdersSet;
    }

    public void setEgiszOrdersSet(Set<EgiszOrderitems> egiszOrdersSet) {
        this.egiszOrdersSet = egiszOrdersSet;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszResearchGroup)) {
            return false;
        }
        EgiszResearchGroup other = (EgiszResearchGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.model.EgiszResearchGroup[ id=" + id + " ]";
    }

}
