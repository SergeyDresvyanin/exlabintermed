/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "EGISZ_EXLAB_RESEARCHTYPES")
@XmlRootElement
public class EgiszResearchtypes implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ISVISIBLE")
    private short isvisible;
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NAME")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refegiszResearchtypes")
    private Set<EgiszOrderitems> egiszOrderitemsSet;

    public EgiszResearchtypes() {
    }

    public EgiszResearchtypes(String code) {
        this.code = code;
    }

    public EgiszResearchtypes(String code, String name) {
        this.code = code;
        this.name = name;

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Set<EgiszOrderitems> getEgiszOrderitemsSet() {
        return egiszOrderitemsSet;
    }

    public void setEgiszOrderitemsSet(Set<EgiszOrderitems> egiszOrderitemsSet) {
        this.egiszOrderitemsSet = egiszOrderitemsSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszResearchtypes)) {
            return false;
        }
        EgiszResearchtypes other = (EgiszResearchtypes) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.EgiszResearchtypes[ code=" + code + " ]";
    }

    public short getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(short isvisible) {
        this.isvisible = isvisible;
    }

}
