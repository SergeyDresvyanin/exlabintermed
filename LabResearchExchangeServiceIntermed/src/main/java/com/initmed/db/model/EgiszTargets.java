/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "EGISZ_EXLAB_TARGETS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EgiszTargets.findAll", query = "SELECT e FROM EgiszTargets e"),
    @NamedQuery(name = "EgiszTargets.findByCode", query = "SELECT e FROM EgiszTargets e WHERE e.code = :code"),
    @NamedQuery(name = "EgiszTargets.findByName", query = "SELECT e FROM EgiszTargets e WHERE e.name = :name"),
    @NamedQuery(name = "EgiszTargets.findByOid", query = "SELECT e FROM EgiszTargets e WHERE e.oid = :oid"),
    @NamedQuery(name = "EgiszTargets.findByIsvisible", query = "SELECT e FROM EgiszTargets e WHERE e.isvisible = :isvisible")})
public class EgiszTargets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NAME")
    private String name;
    @Size(max = 40)
    @Column(name = "OID")
    private String oid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISVISIBLE")
    private short isvisible;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refegiszTargets")
    private Set<EgiszOrders> egiszOrdersSet;

    public EgiszTargets() {
    }

    public EgiszTargets(String code) {
        this.code = code;
    }

    public EgiszTargets(String code, String name, short isvisible) {
        this.code = code;
        this.name = name;
        this.isvisible = isvisible;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public short getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(short isvisible) {
        this.isvisible = isvisible;
    }

    @XmlTransient
    public Set<EgiszOrders> getEgiszOrdersSet() {
        return egiszOrdersSet;
    }

    public void setEgiszOrdersSet(Set<EgiszOrders> egiszOrdersSet) {
        this.egiszOrdersSet = egiszOrdersSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszTargets)) {
            return false;
        }
        EgiszTargets other = (EgiszTargets) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.EgiszTargets[ code=" + code + " ]";
    }
    
}
