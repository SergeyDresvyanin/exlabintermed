/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "EGISZ_TERMS_VERSIONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EgiszTermsVersions.findAll", query = "SELECT e FROM EgiszTermsVersions e"),
    @NamedQuery(name = "EgiszTermsVersions.findById", query = "SELECT e FROM EgiszTermsVersions e WHERE e.id = :id"),
    @NamedQuery(name = "EgiszTermsVersions.findByMistablename", query = "SELECT e FROM EgiszTermsVersions e WHERE e.mistablename = :mistablename"),
    @NamedQuery(name = "EgiszTermsVersions.findByOid", query = "SELECT e FROM EgiszTermsVersions e WHERE e.oid = :oid"),
    @NamedQuery(name = "EgiszTermsVersions.findByVer", query = "SELECT e FROM EgiszTermsVersions e WHERE e.ver = :ver"),
    @NamedQuery(name = "EgiszTermsVersions.findByDescr", query = "SELECT e FROM EgiszTermsVersions e WHERE e.descr = :descr")})
public class EgiszTermsVersions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "MISTABLENAME")
    private String mistablename;
    @Size(max = 255)
    @Column(name = "OID")
    private String oid;
    @Column(name = "VER")
    private Integer ver;
    @Size(max = 755)
    @Column(name = "DESCR")
    private String descr;

    public EgiszTermsVersions() {
    }

    public EgiszTermsVersions(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMistablename() {
        return mistablename;
    }

    public void setMistablename(String mistablename) {
        this.mistablename = mistablename;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EgiszTermsVersions)) {
            return false;
        }
        EgiszTermsVersions other = (EgiszTermsVersions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.model.EgiszTermsVersions[ id=" + id + " ]";
    }
    
}
