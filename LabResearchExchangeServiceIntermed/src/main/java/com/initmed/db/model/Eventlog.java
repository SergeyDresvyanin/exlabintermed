/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "EGISZ_EXLAB_EVENTLOG")
@XmlRootElement
public class Eventlog {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "SERVICE")
    private String serviec;
    @Column(name = "DESCR")
    private String descr;
    @Column(name = "ISEXCEPTION")
    private Boolean isexception;

    public Eventlog() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiec() {
        return serviec;
    }

    public void setServiec(String serviec) {
        this.serviec = serviec;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Boolean getIsexception() {
        return isexception;
    }

    public void setIsexception(Boolean isexception) {
        this.isexception = isexception;
    }

}
