/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "Integration_FRMR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IntegrationFRMR.findAll", query = "SELECT i FROM IntegrationFRMR i"),
    @NamedQuery(name = "IntegrationFRMR.findByDoctorID", query = "SELECT i FROM IntegrationFRMR i WHERE i.doctorID = :doctorID"),
    @NamedQuery(name = "IntegrationFRMR.findByDivisionCode", query = "SELECT i FROM IntegrationFRMR i WHERE i.divisionCode = :divisionCode"),
    @NamedQuery(name = "IntegrationFRMR.findBySpecialityCode", query = "SELECT i FROM IntegrationFRMR i WHERE i.specialityCode = :specialityCode"),
    @NamedQuery(name = "IntegrationFRMR.findByPositionCode", query = "SELECT i FROM IntegrationFRMR i WHERE i.positionCode = :positionCode")})
public class IntegrationFRMR implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DoctorID")
    private Integer doctorID;
    @Size(max = 50)
    @Column(name = "DivisionCode")
    private String divisionCode;
    @Size(max = 50)
    @Column(name = "SpecialityCode")
    private String specialityCode;
    @Size(max = 50)
    @Column(name = "PositionCode")
    private String positionCode;

    public IntegrationFRMR() {
    }

    public IntegrationFRMR(Integer doctorID) {
        this.doctorID = doctorID;
    }

    public Integer getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(Integer doctorID) {
        this.doctorID = doctorID;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getSpecialityCode() {
        return specialityCode;
    }

    public void setSpecialityCode(String specialityCode) {
        this.specialityCode = specialityCode;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (doctorID != null ? doctorID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IntegrationFRMR)) {
            return false;
        }
        IntegrationFRMR other = (IntegrationFRMR) object;
        if ((this.doctorID == null && other.doctorID != null) || (this.doctorID != null && !this.doctorID.equals(other.doctorID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.db.model.IntegrationFRMR[ doctorID=" + doctorID + " ]";
    }
    
}
