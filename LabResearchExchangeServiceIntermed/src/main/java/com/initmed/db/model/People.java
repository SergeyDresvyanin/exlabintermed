/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "PEOPLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "People.findBySnils", query = "SELECT e FROM People e where e.snils =:snils")})
public class People {

    @Id
    @Basic(optional = false)
    @Column(name = "PEOPLE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "ANONYMOUS")
    private Boolean anonymous;
    @Column(name = "LAST_NAME")
    private String last_name;
    @Column(name = "FIRST_NAME")
    private String first_name;
    @Column(name = "MIDDLE_NAME")
    private String middle_name;
    @Column(name = "SEX")
    private Short sex;
    @Column(name = "BIRTH_DATE")
    private Date bDate;
    @Column(name = "CATEGORY")
    private Integer category;
    @Column(name = "CONTINGENT_CODE")
    private Integer contingent_code;
    @Column(name = "SOC_STATUS")
    private Integer soc_status;
    @Column(name = "FAMILY_STATUS")
    private Integer family_status;
    @Column(name = "SNILS")
    private String snils;
    @Column(name = "LPU_CODE")
    private String lpu_code;
    @OneToMany(mappedBy = "people")
    private List<DocAnalysOrder> docAnalysOrder;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "intermedId")
    private TempPersons egiszOrdersSet;

    public People() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }
    
    public String getLpu_code() {
        return lpu_code;
    }

    public void setLpu_code(String lpu_code) {
        this.lpu_code = lpu_code;
    }

    public Date getbDate() {
        return bDate;
    }

    public void setbDate(Date bDate) {
        this.bDate = bDate;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getContingent_code() {
        return contingent_code;
    }

    public void setContingent_code(Integer contingent_code) {
        this.contingent_code = contingent_code;
    }

    public Integer getSoc_status() {
        return soc_status;
    }

    public void setSoc_status(Integer soc_status) {
        this.soc_status = soc_status;
    }

    public Integer getFamily_status() {
        return family_status;
    }

    public void setFamily_status(Integer family_status) {
        this.family_status = family_status;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public TempPersons getEgiszOrdersSet() {
        return egiszOrdersSet;
    }

    public void setEgiszOrdersSet(TempPersons egiszOrdersSet) {
        this.egiszOrdersSet = egiszOrdersSet;
    }

    public List<DocAnalysOrder> getDocAnalysOrder() {
        return docAnalysOrder;
    }

    public void setDocAnalysOrder(List<DocAnalysOrder> docAnalysOrder) {
        this.docAnalysOrder = docAnalysOrder;
    }

}
