/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

/**
 *
 * @author sd199
 */
public class Personnnel {

    private Persons persons;
    private Integer position;
    private Integer speciality;

    public Personnnel(Persons persons, Integer position, Integer speciality) {
        this.persons = persons;
        this.position = position;
        this.speciality = speciality;
    }

    public Persons getPersons() {
        return persons;
    }

    public void setPersons(Persons persons) {
        this.persons = persons;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Integer speciality) {
        this.speciality = speciality;
    }

}
