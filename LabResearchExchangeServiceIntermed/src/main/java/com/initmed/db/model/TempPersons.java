/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "EXLAB_INCOME_PERSONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TempPersons.findBySnils", query = "SELECT e FROM TempPersons e where e.snils = :snils"),
    @NamedQuery(name = "TempPersons.findByOtherMisId", query = "SELECT e FROM TempPersons e where e.otherMisId = :otherMisId")})
public class TempPersons {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "LASTNAME")
    private String lastName;
    @Column(name = "FIRSTNAME")
    private String firstName;
    @Column(name = "SECONDNAME")
    private String secondName;
    @Column(name = "BIRTH_DATE")
    private Date birthDate;
    @Column(name = "SEX")
    private Short sex;
    @Column(name = "SNILS")
    private String snils;

    @JoinColumn(name = "INTERMEDID", referencedColumnName = "PEOPLE_ID")
    @OneToOne
    private People intermedId;
    @Column(name = "PROCESSED")
    private Short processed;
    @Column(name = "OTHERMISID")
    private String otherMisId;
    @Column(name = "PERSONSUUID")
    private String personsUUID;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refpersons")
    private Set<EgiszOrders> egiszOrdersSet;

    public TempPersons() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public People getIntermedId() {
        return intermedId;
    }

    public void setIntermedId(People intermedId) {
        this.intermedId = intermedId;
    }

    public Short getProcessed() {
        return processed;
    }

    public void setProcessed(Short processed) {
        this.processed = processed;
    }

    public String getOtherMisId() {
        return otherMisId;
    }

    public void setOtherMisId(String otherMisId) {
        this.otherMisId = otherMisId;
    }

    public Set<EgiszOrders> getEgiszOrdersSet() {
        return egiszOrdersSet;
    }

    public void setEgiszOrdersSet(Set<EgiszOrders> egiszOrdersSet) {
        this.egiszOrdersSet = egiszOrdersSet;
    }

    public String getPersonsUUID() {
        return personsUUID;
    }

    public void setPersonsUUID(String personsUUID) {
        this.personsUUID = personsUUID;
    }

}
