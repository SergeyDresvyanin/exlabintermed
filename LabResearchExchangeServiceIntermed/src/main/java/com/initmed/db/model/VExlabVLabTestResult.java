/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "EXLAB_V_LABTEST_RESULT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VExlabVLabTestResult.finByOrderAimId", query = "SELECT e FROM VExlabVLabTestResult e where e.orderAimId = :aimId")})
public class VExlabVLabTestResult {

    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "RESDIGIT")
    private Double resDigit;
    @Column(name = "RESTEXT")
    private String resText;
    @Column(name = "ORDER_AIM_ID")
    private Integer orderAimId;
    @Column(name = "ADATE")
    private Date adate;
    @Column(name = "MINVAL")
    private Double minVal;
    @Column(name = "MAXVAL")
    private Double maxVal;

    @Column(name = "PDFFILE")
    private byte[] docFile;
    @Column(name = "ORGSIGN")
    private byte[] orgsign;
    @Column(name = "DOCSIGN")
    private byte[] docsign;
    private Integer docid;
    private String docsnils;
    private String docfname;
    private String doclname;
    private String docmname;
    private Integer docspcode;
    private Integer docposcocde;
    private String docdivcode;
    private Integer orgid;
    private String orgsnils;
    private String orgfname;
    private String orglname;
    private String orgmname;
    private Integer orgspcode;
    private Integer orgposcocde;
    private String orgdivcode;

    @ManyToOne(optional = false)
    @JoinColumn(name = "PEOPLE_ID", referencedColumnName = "PEOPLE_ID")
    private Persons persons;
    @ManyToOne(optional = false)
    @JoinColumn(name = "REFMEASUREMENTTYPE", referencedColumnName = "ID")
    private EgiszMeasureTypes refMeasureType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "REFRESMEASURE", referencedColumnName = "ID")
    private EgiszMeasure egiszMeasure;

    public VExlabVLabTestResult() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getResDigit() {
        return resDigit;
    }

    public void setResDigit(Double resDigit) {
        this.resDigit = resDigit;
    }

    public String getResText() {
        return resText;
    }

    public void setResText(String resText) {
        this.resText = resText;
    }

    public EgiszMeasureTypes getRefMeasureType() {
        return refMeasureType;
    }

    public void setRefMeasureType(EgiszMeasureTypes refMeasureType) {
        this.refMeasureType = refMeasureType;
    }

    public Double getMinVal() {
        return minVal;
    }

    public void setMinVal(Double minVal) {
        this.minVal = minVal;
    }

    public Double getMaxVal() {
        return maxVal;
    }

    public void setMaxVal(Double maxVal) {
        this.maxVal = maxVal;
    }

    public Persons getPersons() {
        return persons;
    }

    public void setPersons(Persons persons) {
        this.persons = persons;
    }

    public EgiszMeasure getEgiszMeasure() {
        return egiszMeasure;
    }

    public void setEgiszMeasure(EgiszMeasure egiszMeasure) {
        this.egiszMeasure = egiszMeasure;
    }

    public Date getAdate() {
        return adate;
    }

    public void setAdate(Date adate) {
        this.adate = adate;
    }

    public Integer getOrderAimId() {
        return orderAimId;
    }

    public void setOrderAimId(Integer orderAimId) {
        this.orderAimId = orderAimId;
    }

    public byte[] getDocFile() {
        return docFile;
    }

    public void setDocFile(byte[] docFile) {
        this.docFile = docFile;
    }

    public byte[] getOrgsign() {
        return orgsign;
    }

    public void setOrgsign(byte[] orgsign) {
        this.orgsign = orgsign;
    }

    public byte[] getDocsign() {
        return docsign;
    }

    public void setDocsign(byte[] docsign) {
        this.docsign = docsign;
    }

    public String getDocsnils() {
        return docsnils;
    }

    public void setDocsnils(String docsnils) {
        this.docsnils = docsnils;
    }

    public String getDocfname() {
        return docfname;
    }

    public void setDocfname(String docfname) {
        this.docfname = docfname;
    }

    public String getDoclname() {
        return doclname;
    }

    public void setDoclname(String doclname) {
        this.doclname = doclname;
    }

    public String getDocmname() {
        return docmname;
    }

    public void setDocmname(String docmname) {
        this.docmname = docmname;
    }

    public String getDocdivcode() {
        return docdivcode;
    }

    public void setDocdivcode(String docdivcode) {
        this.docdivcode = docdivcode;
    }

    public String getOrgsnils() {
        return orgsnils;
    }

    public void setOrgsnils(String orgsnils) {
        this.orgsnils = orgsnils;
    }

    public String getOrgfname() {
        return orgfname;
    }

    public void setOrgfname(String orgfname) {
        this.orgfname = orgfname;
    }

    public String getOrglname() {
        return orglname;
    }

    public void setOrglname(String orglname) {
        this.orglname = orglname;
    }

    public String getOrgmname() {
        return orgmname;
    }

    public void setOrgmname(String orgmname) {
        this.orgmname = orgmname;
    }

    public Integer getDocspcode() {
        return docspcode;
    }

    public void setDocspcode(Integer docspcode) {
        this.docspcode = docspcode;
    }

    public Integer getDocposcocde() {
        return docposcocde;
    }

    public void setDocposcocde(Integer docposcocde) {
        this.docposcocde = docposcocde;
    }

    public Integer getOrgspcode() {
        return orgspcode;
    }

    public void setOrgspcode(Integer orgspcode) {
        this.orgspcode = orgspcode;
    }

    public Integer getOrgposcocde() {
        return orgposcocde;
    }

    public String getOrgdivcode() {
        return orgdivcode;
    }

    public void setOrgdivcode(String orgdivcode) {
        this.orgdivcode = orgdivcode;
    }

    public Integer getDocid() {
        return docid;
    }

    public void setDocid(Integer docid) {
        this.docid = docid;
    }

    public Integer getOrgid() {
        return orgid;
    }

    public void setOrgid(Integer orgid) {
        this.orgid = orgid;
    }

}
