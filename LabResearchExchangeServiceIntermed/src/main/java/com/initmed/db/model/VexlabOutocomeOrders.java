/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.db.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "V_EXLAB_OUTCOMEORDERS")
public class VexlabOutocomeOrders {

    @Id
    private Long id;
    @Column(name = "PEOPLE_ID")
    private Long peopleId;
    @Column(name = "COLLECT_DATE")
    private Date collectDate;
    private String Org_Guid;
    @Column(name = "FIRST_NAME")
    private String docFirstName;
    @Column(name = "LAST_NAME")
    private String docLastName;
    @Column(name = "MIDDLE_NAME")
    private String docMiddleName;
    @Column(name = "SNILS")
    private String docSnils;
    @Column(name = "REFCONTAINERTYPES")
    private Integer refContainertypes;
    @Column(name = "REFMATERIALS")
    private Integer refMaterials;
    @Column(name = "RESEARCHTYPES")
    private String researchTypes;
    @Column(name = "paymenttype")
    private String paymenttype;
    @Column(name = "mkb")
    private String mkb;
    @Column(name = "diagndate")
    private Date diagndate;

    public VexlabOutocomeOrders() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Long peopleId) {
        this.peopleId = peopleId;
    }

    public Integer getRefMaterials() {
        return refMaterials;
    }

    public void setRefMaterials(Integer refMaterials) {
        this.refMaterials = refMaterials;
    }

    public Date getCollectDate() {
        return collectDate;
    }

    public void setCollectDate(Date collectDate) {
        this.collectDate = collectDate;
    }

    public String getOrg_Guid() {
        return Org_Guid;
    }

    public void setOrg_Guid(String Org_Guid) {
        this.Org_Guid = Org_Guid;
    }

    public String getDocFirstName() {
        return docFirstName;
    }

    public void setDocFirstName(String docFirstName) {
        this.docFirstName = docFirstName;
    }

    public String getDocLastName() {
        return docLastName;
    }

    public void setDocLastName(String docLastName) {
        this.docLastName = docLastName;
    }

    public String getDocMiddleName() {
        return docMiddleName;
    }

    public void setDocMiddleName(String docMiddleName) {
        this.docMiddleName = docMiddleName;
    }

    public String getDocSnils() {
        return docSnils;
    }

    public void setDocSnils(String docSnils) {
        this.docSnils = docSnils;
    }

    public Integer getRefContainertypes() {
        return refContainertypes;
    }

    public void setRefContainertypes(Integer refContainertypes) {
        this.refContainertypes = refContainertypes;
    }

    public String getResearchTypes() {
        return researchTypes;
    }

    public void setResearchTypes(String researchTypes) {
        this.researchTypes = researchTypes;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public String getMkb() {
        return mkb;
    }

    public void setMkb(String mkb) {
        this.mkb = mkb;
    }

    public Date getDiagndate() {
        return diagndate;
    }

    public void setDiagndate(Date diagndate) {
        this.diagndate = diagndate;
    }

}
