/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.exceptions;

/**
 *
 * @author sd199
 */
public class WrongDateIntervalException extends Exception {

    /**
     * Creates a new instance of <code>WrongDateIntervalException</code> without
     * detail message.
     */
    public WrongDateIntervalException() {
    }

    /**
     * Constructs an instance of <code>WrongDateIntervalException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public WrongDateIntervalException(String msg) {
        super(msg);
    }
}
