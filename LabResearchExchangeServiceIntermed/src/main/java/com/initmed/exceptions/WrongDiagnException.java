/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.exceptions;

/**
 *
 * @author sd199
 */
public class WrongDiagnException extends Exception {

    /**
     * Creates a new instance of <code>WrongDiagnException</code> without detail
     * message.
     */
    public WrongDiagnException() {
    }

    /**
     * Constructs an instance of <code>WrongDiagnException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public WrongDiagnException(String msg) {
        super(msg);
    }
}
