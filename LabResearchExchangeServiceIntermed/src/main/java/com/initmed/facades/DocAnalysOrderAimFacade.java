/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.DocAnalysOrderAim;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author sd199
 */
@Stateless
public class DocAnalysOrderAimFacade extends AbstractFacade<DocAnalysOrderAim> {

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocAnalysOrderAimFacade() {
        super(DocAnalysOrderAim.class);
    }
    
}
