/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.DocAnalysOrder;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class DocAnalysOrderFacade extends AbstractFacade<DocAnalysOrder> {

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocAnalysOrderFacade() {
        super(DocAnalysOrder.class);
    }

    public Integer getResearch(String res) {
        Query q = em.createNativeQuery("SELECT TOP 1 AIMID FROM EXLAB_ORDER_CONVERTER WHERE RESEARCHTYPES =?");
        q.setParameter(1, res);
        List<Integer> str = q.getResultList();
        return str.isEmpty() ? null : (int) str.get(0);
    }

    public Integer getMaterial(String res, Integer material) {
        Query q = em.createNativeQuery("SELECT TOP 1 MATERIAL FROM EXLAB_ORDER_CONVERTER WHERE RESEARCHTYPES =? and REFMATERIALS=?");
        q.setParameter(1, res);
        q.setParameter(2, material);
        List<Integer> str = q.getResultList();
        return str.isEmpty() ? null : (int) str.get(0);
    }

    public Integer calculateDoc(String guid) {
        Query q = em.createNativeQuery("SELECT  \n"
                + " COALESCE(idd.DoctorId,897) \n"
                + " FROM Integration_DefaultDoctors idd where idd.ORG_ID = ? \n");
        q.setParameter(1, guid);        
        List<Integer> str = q.getResultList();
        return str.isEmpty() ? 897 : (int) str.get(0);
    }

}
