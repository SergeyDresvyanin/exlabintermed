/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.EgiszOrders;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class EgiszOrdersFacade extends AbstractFacade<EgiszOrders> {

    private static final int COUNT_ORDERS = 10;

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EgiszOrdersFacade() {
        super(EgiszOrders.class);
    }

    public Integer updateBioOrders() {
        Query bactBoundles = em.createNativeQuery("INSERT INTO EGISZ_EXLAB_ORDERS(REFORDER, REFEGISZ_TARGETS, SOURCE, DIAGNOSISDATE, INCOME, PROCESSEDINBD)\n"
                + "SELECT distinct \n"
                + "DO.ID,\n"
                + "doc_frmr.DivisionCode REFEGISZ_TARGETS,\n"
                + "doc_frmr.DivisionCode SOURCE,\n"
                + "DO.[DATE] DIAGNOSISDATE,\n"
                + "0 INCOME,\n"
                + "1 PROCESSEDINBD\n"
                + "FROM LABS_PERS_RES LA\n"
                + "INNER JOIN PEOPLE P ON P.PEOPLE_ID = LA.PEOPLE_ID\n"
                + "INNER JOIN LABS_PERS_RES_PARAMS LPRA ON LPRA.PERS_RES_ID = LA.ID\n"
                + "INNER JOIN EXLAB_ORDER_CONVERTER EOC ON EOC.AIMID = LA.AIM_ID AND EOC.MATERIAL = LA.MATERIAL_ID\n"
                + "INNER JOIN EXLAB_ORDER_RESULT_CONVERTER ER ON ER.PARAMID = LPRA.PARAM_ID  AND ER.SEX = P.SEX\n"
                + "INNER JOIN DOCTOR_ANALYS_ORDER_AIMS DAA ON DAA.ID = LA.ORDER_AIM_ID\n"
                + "INNER JOIN DOCTOR_ANALYS_ORDER DO ON DO.ID = DAA.ORDER_ID\n"
                + "INNER JOIN Integration_Files f ON LA.ID = f.EntityID\n"
                + "JOIN DICT_DOCTORS doc ON doc.ID = f.SignatureDoctorID\n"
                + "JOIN Integration_FRMR doc_frmr ON doc_frmr.DoctorID = doc.ID\n"
                + "JOIN DICT_DOCTORS org ON org.ID = f.SignatureDoctorID\n"
                + "JOIN Integration_FRMR org_frmr ON org_frmr.DoctorID = org.ID\n"
                + "LEFT JOIN EGISZ_EXLAB_ORDERS EO ON EO.REFORDER = DO.ID\n"
                + "WHERE P.SNILS IS NOT NULL\n"
                + "AND P.BIRTH_DATE IS NOT NULL\n"
                + "AND F.OrgSignature IS NOT NULL\n"
                + "AND f.EntityTypeId = 11\n"
                + "AND P.FIRST_NAME IS NOT NULL\n"
                + "AND P.LAST_NAME IS NOT NULL\n"
                + "AND P.MIDDLE_NAME IS NOT NULL\n"
                + "AND EO.ID IS NULL\n"
                + "AND LA.COLLECT_DATE>'2020-05-05'\n"
                + "and (LPRA.RESULT_VALUE is not null or (LPRA.RESULT_TEXT is not null and len(LPRA.RESULT_TEXT)>0))");
        int rowsCount = bactBoundles.executeUpdate();
        return rowsCount;
    }

    public String getResearch(Integer aimId) throws Exception {
        Query q = em.createNativeQuery("select top 1 eo.RESEARCHTYPES  from EXLAB_ORDER_CONVERTER eo\n"
                + "where eo.AIMID = ?");
        q.setParameter(1, aimId);
        List<String> s = q.getResultList();
        if (s.isEmpty()) {
            throw new Exception("Перекодировка услуги не настроена");
        }
        return s.get(0);
    }

    public List<EgiszOrders> getNotSendedBoundle() {
        Query bundles = em.createNativeQuery("SELECT DISTINCT top 10 E.*\n"
                + "FROM EGISZ_EXLAB_ORDERS E\n"
                + "INNER JOIN EGISZ_EXLAB_ORDERITEMS EO ON EO.REFEGISZ_ORDERS=E.ID \n"
                + "inner join EXLAB_V_LABTEST_RESULT vi on vi.ORDER_AIM_ID = eo.REFORDEDRAIM \n"
                + "WHERE e.INCOME =0 and  E.ORDERUUID IS NULL AND E.ERRORMSG IS NULL and DATEDIFF(DAY,e.CREATETIME,GETDATE())<90\n"
                + " ORDER BY E.CREATETIME DESC", EgiszOrders.class);
        return (List<EgiszOrders>) bundles.setMaxResults(5).getResultList();
    }
    
    public List<EgiszOrders> getAllNotSentBoundles() {
        Query bundles = em.createNativeQuery("SELECT DISTINCT E.*\n"
                + "FROM EGISZ_EXLAB_ORDERS E\n"
                + "INNER JOIN EGISZ_EXLAB_ORDERITEMS EO ON EO.REFEGISZ_ORDERS=E.ID \n"
                + "inner join EXLAB_V_LABTEST_RESULT vi on vi.ORDER_AIM_ID = eo.REFORDEDRAIM \n"
                + "WHERE e.INCOME =0 and  E.ORDERUUID IS NULL AND E.ERRORMSG IS NULL and DATEDIFF(DAY,e.CREATETIME,GETDATE())<90\n"
                + " ORDER BY E.CREATETIME DESC", EgiszOrders.class);
        return (List<EgiszOrders>) bundles.setMaxResults(5).getResultList();
    }

    public List<EgiszOrders> getBoundleInRequest() {
        Query bundles = em.createNamedQuery("EgiszOrders.getBoundleInRequest");
        return bundles.setFirstResult(1).getResultList();
    }

    public List<EgiszOrders> getReadyResults() {
        Query bundles = em.createNativeQuery("SELECT DISTINCT top 5 EXOD.* FROM EGISZ_EXLAB_ORDERS EXOD\n"
                + " INNER JOIN EGISZ_EXLAB_ORDERITEMS EXODI ON EXODI.REFEGISZ_ORDERS = EXOD.ID\n"
                + " INNER JOIN EXLAB_V_LABTEST_RESULT V ON EXODI.REFORDEDRAIM=V.ORDER_AIM_ID\n"
                + " INNER JOIN LABS_PERS_RES LR ON LR.ORDER_AIM_ID = EXODI.REFORDEDRAIM\n"
                + " INNER JOIN LABS_PERS_RES_PARAMS LPRA ON LPRA.PERS_RES_ID = LR.ID\n"
                + " INNER JOIN EXLAB_ORDER_CONVERTER EOC ON EOC.AIMID = LR.AIM_ID AND EOC.MATERIAL = LR.MATERIAL_ID\n"
                + " INNER JOIN PEOPLE P ON P.PEOPLE_ID = LR.PEOPLE_ID\n"
                + " INNER JOIN EXLAB_ORDER_RESULT_CONVERTER ER ON ER.PARAMID = LPRA.PARAM_ID  AND ER.SEX = P.SEX\n"
                + " WHERE EXOD.INCOME = 1 AND \n"
                + " (LPRA.RESULT_VALUE is not null or (LPRA.RESULT_TEXT is not null and len(LPRA.RESULT_TEXT)>0)) AND \n"
                + " EXOD.guid is null and EXOD.errormsg is null", EgiszOrders.class);
        return bundles.setFirstResult(1).getResultList();
    }

    public List<EgiszOrders> getLastBoundles() {
        Query bundles = em.createNativeQuery("select top 15 * from EGISZ_EXLAB_ORDERS eo\n"
                + "order by eo.id desc", EgiszOrders.class);
        return bundles.setMaxResults(15).getResultList();
    }

    public EgiszOrders getOrderByGUID(String guid) {
        Query q = em.createNamedQuery("EgiszOrders.getByOrderUUID", EgiszOrders.class);
        q.setParameter("guid", guid);
        List<EgiszOrders> eList = q.getResultList();
        if (eList == null || eList.isEmpty()) {
            return null;
        }
        return eList.get(0);
    }
    
    public EgiszOrders getOrderByDiagnOrderUUID(String guid) {
        Query q = em.createNamedQuery("EgiszOrders.getByDiagOrderUUID", EgiszOrders.class);
        q.setParameter("guid", guid);
        List<EgiszOrders> eList = q.getResultList();
        if (eList == null || eList.isEmpty()) {
            return null;
        }
        return eList.get(0);
    }

    public void setAppoinmentStatus(Integer appId, Integer status) {
        Query q = em.createNativeQuery("UPDATE imt_External_Appoinments SET StatusID =? where id = ?");
        q.setParameter(1, appId);
        q.setParameter(2, status);
        q.executeUpdate();
    }
}
