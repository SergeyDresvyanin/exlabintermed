/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.EgiszOrderstatus;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author User
 */
@Stateless
public class EgiszOrderstatusFacade extends AbstractFacade<EgiszOrderstatus> {

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EgiszOrderstatusFacade() {
        super(EgiszOrderstatus.class);
    }

    public EgiszOrderstatus getByStatusName(String name) {
        Query bundles = em.createNamedQuery("EgiszOrderstatus.findByCode");
        bundles.setParameter("code", name.toUpperCase());
        return (EgiszOrderstatus) bundles.getSingleResult();
    }

}
