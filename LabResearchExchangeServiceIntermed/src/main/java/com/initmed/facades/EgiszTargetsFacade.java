/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.EgiszTargets;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author User
 */
@Stateless
public class EgiszTargetsFacade extends AbstractFacade<EgiszTargets> {

    private static final int COUNT_ORDERS = 10;

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EgiszTargetsFacade() {
        super(EgiszTargets.class);
    }

    public List<EgiszTargets> loadByOID(String oid) {
        Query q = em.createNativeQuery("SELECT  *\n"
                + "  FROM [Intermed-spb].[dbo].[EGISZ_EXLAB_TARGETS]\n"
                + "  WHERE [OID] LIKE ?", EgiszTargets.class);
        q.setParameter(1, oid + "%");
        return q.getResultList();
    }

}
