/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.EgiszTargets;
import com.initmed.db.model.EgiszTermsVersions;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class EgiszVersionTargets extends AbstractFacade<EgiszTermsVersions> {

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EgiszVersionTargets() {
        super(EgiszTermsVersions.class);
    }

    public Integer loadVersionByOID(String OID) {
        Query q = em.createNamedQuery("EgiszTermsVersions.findByOid");
        q.setParameter("oid", OID);
        return ((EgiszTermsVersions) q.getSingleResult()).getVer();
    }
}
