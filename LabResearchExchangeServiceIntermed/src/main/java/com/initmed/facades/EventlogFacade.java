/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.Eventlog;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author sd199
 */
@Stateless
public class EventlogFacade extends AbstractFacade<Eventlog> {

    private static final String CREATE_ORDER = "Отправка заявки на исследование";

    private static final String CREATE_NOT_PRE_ORDER = "Отправка исследований без заявки";

    private static final String IMPORT_RESEARCH = "Подгружено входящих исследований ";

    private static final String EXPORT_RESULT = "Выгружен результат по ";

    private static final String IMPORT_FROM_DB = "Исследований с БД подгружено ";

    private static final String IMPORT_TO_DB = "Исследований в БД сохранены ";

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EventlogFacade() {
        super(Eventlog.class);
    }

    public void createLogMessage(String msg, boolean isExcep) {
        Eventlog el = new Eventlog();
        el.setServiec("FHIR");
        el.setDescr(msg);
        el.setIsexception(isExcep);
        create(el);
    }

    public void reserchesNotPreOrderCreated(int researCount) {
        createLogMessage(CREATE_NOT_PRE_ORDER + researCount, false);
    }

    public void reserchesLoaded(int researCount) {
        createLogMessage(IMPORT_FROM_DB + researCount, false);
    }

    public void reserchesImported(int researCount) {
        createLogMessage(IMPORT_RESEARCH + researCount, false);
    }

    public void reserchesExported(int researCount) {
        createLogMessage(EXPORT_RESULT + researCount, false);
    }

    public void reserchesImportToDb(int researCount) {
        createLogMessage(IMPORT_TO_DB + researCount, false);
    }

    public void reserchesOrder(int researCount) {
        createLogMessage(CREATE_ORDER + researCount, false);
    }

}
