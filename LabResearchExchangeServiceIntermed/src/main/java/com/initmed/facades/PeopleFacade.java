/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.People;
import com.initmed.db.model.TempPersons;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class PeopleFacade extends AbstractFacade<People> {

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PeopleFacade() {
        super(People.class);
    }

    public People syncPeople(TempPersons tp) {
        People p = loadBySnils(tp);
        if (p == null) {
            p = new People();
            p.setAnonymous(Boolean.FALSE);
            p.setLast_name(tp.getLastName());
            p.setFirst_name(tp.getFirstName());
            p.setMiddle_name(tp.getSecondName());
            p.setSex(tp.getSex());
            p.setLpu_code("0");
            p.setbDate(tp.getBirthDate());
            p.setCategory(12658);
            p.setContingent_code(15127);
            p.setSoc_status(12748);
            p.setFamily_status(12730);
            if (tp.getSnils() != null && tp.getSnils().length() > 9) {
                p.setSnils(tp.getSnils().substring(0, 3) + "-" + tp.getSnils().substring(3, 6) + "-" + tp.getSnils().substring(6, 9) + "-" + tp.getSnils().substring(9));
            }
            create(p);
        }
        return p;
    }

    public People loadBySnils(TempPersons tp) {
        Query q = em.createNamedQuery("People.findBySnils");
        if (tp.getSnils() != null && tp.getSnils().length() > 9) {
            q.setParameter("snils", tp.getSnils().substring(0, 3) + "-" + tp.getSnils().substring(3, 6) + "-" + tp.getSnils().substring(6, 9) + "-" + tp.getSnils().substring(9));
            List<People> pList = q.getResultList();
            return pList.isEmpty() ? null : pList.get(0);
        } else {
            return null;
        }
    }
}
