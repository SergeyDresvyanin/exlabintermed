/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.StringDt;
import com.initmed.db.model.People;
import com.initmed.db.model.TempPersons;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class TempPersonsFacade extends AbstractFacade<TempPersons> {

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @EJB
    private PeopleFacade peopleFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TempPersonsFacade() {
        super(TempPersons.class);
    }

    public TempPersons syncPatient(Patient p) {
        String snils = null;
        TempPersons res = null;
        String otherMisId = null;

        List<IdentifierDt> idtList = p.getIdentifier();
        for (IdentifierDt x : idtList) {
            if (x.getSystem().equals("urn:oid:1.2.643.5.1.13.2.7.100.5")) {
                otherMisId = x.getValue();
            }
            if (x.getSystem().equals("urn:oid:1.2.643.2.69.1.1.1.6.223")) {
                snils = x.getValue();
            }
        }
        if (snils != null) {
            Query q = em.createNamedQuery("TempPersons.findBySnils", TempPersons.class);
            q.setParameter("snils", snils);
            List<TempPersons> tpList = q.getResultList();
            res = tpList.isEmpty() ? null : tpList.get(0);
        } else {
            Query q = em.createNamedQuery("TempPersons.findByOtherMisId", TempPersons.class);
            q.setParameter("otherMisId", otherMisId);
            List<TempPersons> tpList = q.getResultList();
            res = tpList.isEmpty() ? null : tpList.get(0);
        }
        if (res == null) {
            String lastName = null;
            String firstName = null;
            String secondName = null;

            for (HumanNameDt name : p.getName()) {
                if (name.getFamily() != null) {
                    Iterator<StringDt> iterator = name.getFamily().iterator();
                    lastName = iterator.next().getValue();
                    StringDt st = null;
                    if (iterator.hasNext()) {
                        st = iterator.next();
                    }
                    if (st != null) {
                        secondName = st.getValue();
                    }
                }
                if (name.getGiven() != null) {
                    Iterator<StringDt> iterator = name.getGiven().iterator();
                    firstName = iterator.next().getValue();
                }
            }
            res = new TempPersons();
            res.setSnils(snils);
            res.setOtherMisId(otherMisId);
            res.setLastName(lastName);
            res.setFirstName(firstName);
            String patId = p.getId().getValue();
            patId = patId.substring(0, patId.indexOf("/_"));
            res.setPersonsUUID(patId);
            if (secondName != null) {
                res.setSecondName(secondName);
            }
            if (p.getGender().equals("male")) {
                res.setSex((short) 1);
            } else {
                res.setSex((short) 0);
            }
            res.setBirthDate(p.getBirthDate());
            create(res);
        }
        if (res.getIntermedId() == null) {
            People people = peopleFacade.syncPeople(res);
            System.out.println("Persons id " + people.getId());
            people.setEgiszOrdersSet(res);
            res.setIntermedId(people);
            edit(res);
        }
        return res;

    }

}
