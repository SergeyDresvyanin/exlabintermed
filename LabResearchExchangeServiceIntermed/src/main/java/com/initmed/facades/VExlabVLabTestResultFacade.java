/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.facades;

import com.initmed.db.model.VExlabVLabTestResult;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class VExlabVLabTestResultFacade extends AbstractFacade<VExlabVLabTestResult> {

    @PersistenceContext(unitName = "com.initmed_LabResearchExchangeService_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VExlabVLabTestResultFacade() {
        super(VExlabVLabTestResult.class);
    }

    public List<VExlabVLabTestResult> loadResults(Long reftest) {
        Query q = em.createNamedQuery("VExlabVLabTestResult.finByOrderAimId", VExlabVLabTestResult.class);
        q.setParameter("aimId", reftest);
        return q.getResultList();
    }

}
