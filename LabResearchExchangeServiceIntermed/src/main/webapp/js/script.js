/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadData();
});


function loadData() {

    //500 последних заявок на исследование
    var table = $("#caseslist").DataTable({
        "ajax": "api/admin/rowlist",
        'ordering': false,
        'info': false,
        'bLengthChange': false,
        'searching': false,
        'retrieve': true,
        "columns": [
            {"data": "id"},
            {"data": "name", "defaultContent": ""},
            {"data": "error", "defaultContent": ""},
            {"data": "date"},
            {"data": "research"},
            {"data": "id", render: function (data, type, row) {
                    return "<button type='button' class='btn btn-primary btn-action' onclick='callDialog(&quot api/admin/order/request/" + data + "&quot)'>Заявка</button>";
                }},
            {"data": "id", render: function (data, type, row) {
                    return "<button type='button' class='btn btn-primary btn-action' onclick='callDialog(&quot api/admin/order/status/" + data + "&quot)'>Статус</button>";
                }},
            {"data": "id", render: function (data, type, row) {
                    return "<button type='button' class='btn btn-primary btn-action' onclick='callDialog(&quot api/admin/order/result/" + data + "&quot)'>Отправить</button>";
                }}
        ]
    });
}

function updateDataTable() {
    $('#caseslist').DataTable().ajax.reload();
}

function loadingStart() {
    $("#loadingbar").modal("show");
}

function loadingEnd() {
    $("#loadingbar").modal("hide");
}


function execGetCall(url) {
    $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () {
            loadingStart();
        },
        success: function (res) {
            loadingEnd();
            $('.modal-body').text("Успешно");
            $('#messagedialog').modal('show');
            updateDataTable();
        },
        error: function (request, status, error) {
            loadingEnd();
            $('.modal-body').text(request.responseText);
            $('#messagedialog').modal('show');
            $('.modal-body').find('.modal-title').text("Произошла ошибка");
        }
    });
}


function callDialog(url) {
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'text/plain',
        beforeSend: function () {
            loadingStart();
        },
        success: function (res) {
            // get the ajax response data
            var data = res.body;
            // update modal content
            $('#messagedialogbody').text("Успешно");
            // show modal
            loadingEnd();
            $('#messagedialog').modal('show');

        },
        error: function (request, status, error) {
            loadingEnd();
            $('#messagedialogbody').text(request.responseText);
            $('#messagedialog').modal('show');
        }
    });
}