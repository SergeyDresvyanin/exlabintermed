/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.termsservice.controller;

import com.initmed.termsservice.exceptions.ParamUrlNotAddedException;
import com.initmed.termsservice.exceptions.TermNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author sd199
 */
@Singleton
@Startup
public class JobManager {

    @EJB
    private TermsSync termsSync;

//    @Schedule(minute = "*/1", hour = "*", persistent = false)
//    public void executeTestGetVersion() {
//        try {
//            String ver = termsClientImpl.loadVersion("1.2.643.2.69.1.1.1.36");
//            System.out.println(ver);
//        } catch (ParamUrlNotAddedException ex) {
//            Logger.getLogger(JobManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
    @Schedule(hour = "*/23", persistent = false)
    public void executeGetTerm() throws ParamUrlNotAddedException {
        termsSync.syncSimpleTerms();
        try {
            Logger.getLogger(JobManager.class.getName()).log(Level.INFO, null, "Обновление простых справочников");
            termsSync.syncSimpleTerms();
            Logger.getLogger(JobManager.class.getName()).log(Level.INFO, null, "Обновление egisz_targets");
            termsSync.updateEgiszTargets();
            Logger.getLogger(JobManager.class.getName()).log(Level.INFO, null, "Обновление видов услуг");
            termsSync.updateEgiszResearchtypes();
            Logger.getLogger(JobManager.class.getName()).log(Level.INFO, null, "Обновление типов ииледования");
            termsSync.updateEgiszResearchType();
            Logger.getLogger(JobManager.class.getName()).log(Level.INFO, null, "Обновление статусов заявки");
            termsSync.updateEgiszOrderStatus();
        } catch (TermNotFoundException ex) {
            Logger.getLogger(JobManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
