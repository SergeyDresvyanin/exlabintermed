/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.termsservice.controller;

import static com.initmed.termsservice.controller.TermsSync.EGISZ_CASECLASS;
import static com.initmed.termsservice.controller.TermsSync.EGISZ_MEDSERVICEPROFILE;
import static com.initmed.termsservice.controller.TermsSync.EGISZ_ORDERSTATUS;
import static com.initmed.termsservice.controller.TermsSync.EGISZ_RESEARCHTYPE;
import static com.initmed.termsservice.controller.TermsSync.EGISZ_RESEARCHTYPES;
import static com.initmed.termsservice.controller.TermsSync.EGISZ_SPECIALITIES;
import static com.initmed.termsservice.controller.TermsSync.EGISZ_TARGETS;
import static com.initmed.termsservice.controller.TermsSync.EGISZ_WORK_POSITIONS;
import com.initmed.termsservice.db.jpa.EgiszTermsVersions;
import com.initmed.termsservice.db.jpa.EgiszTermsVersionsFacade;
import com.initmed.termsservice.exceptions.ParamUrlNotAddedException;
import com.initmed.termsservice.exceptions.TermNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sd199
 */
@Path("/")
@Stateless
public class RestController {

    @EJB
    private EgiszTermsVersionsFacade egiszTermsVersionsFacade;
    @EJB
    private TermsSync termsSync;

    @Path("/getallterms")
    @GET
    public Response getAllTerms() throws JSONException {
        List<EgiszTermsVersions> list = egiszTermsVersionsFacade.findAll();
        JSONArray resp = new JSONArray();
        for (EgiszTermsVersions x : list) {
            JSONObject jo = new JSONObject();
            jo.put("id", x.getId());
            jo.put("name", x.getDescr());
            jo.put("oid", x.getOid());
            jo.put("ver", x.getVer());
            resp.put(jo);
        }
        return Response.ok().entity(resp.toString()).build();
    }

    @Path("/updateterm/{oid}")
    @GET
    public Response updateTerm(@PathParam("oid") String termoid) {
        try {
            if (termoid.equals(EGISZ_TARGETS)) {
                termsSync.updateEgiszTargets();
            } else if (termoid.equals(EGISZ_RESEARCHTYPES)) {
                termsSync.updateEgiszResearchtypes();
            } else if (termoid.equals(EGISZ_RESEARCHTYPE)) {
                termsSync.updateEgiszResearchType();
            } else if (termoid.equals(EGISZ_CASECLASS)) {
                termsSync.updateEgiszCaseClass();
            } else if (termoid.equals(EGISZ_ORDERSTATUS)) {
                termsSync.updateEgiszOrderStatus();
            } else if (termoid.equals(EGISZ_MEDSERVICEPROFILE)) {
                termsSync.updateEgiszMedServiceProfile();
            } else {
                termsSync.syncSimpleTermByOid(termoid);
            }
            return Response.ok().build();
        } catch (ParamUrlNotAddedException | TermNotFoundException ex) {
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }

    @Path("/updateall")
    @GET
    public Response updateAll() {
        try {
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, "Обновление простых справочников");
            termsSync.syncSimpleTerms();
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, "Обновление egisz_targets");
            termsSync.updateEgiszTargets();
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, "Обновление видов услуг");
            termsSync.updateEgiszResearchtypes();
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, "Обновление статусов заявки");
            termsSync.updateEgiszOrderStatus();
            return Response.ok().build();
        } catch (ParamUrlNotAddedException | TermNotFoundException ex) {
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
}
