/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.termsservice.controller;

import com.initmed.termsservice.db.jpa.EgiszTermsVersions;
import com.initmed.termsservice.db.jpa.EgiszTermsVersionsFacade;
import com.initmed.termsservice.exceptions.ParamUrlNotAddedException;
import com.initmed.termsservice.exceptions.TermNotFoundException;
import com.initmed.termsservice.termsclient.TermsClient;
import com.initmed.termsservice.termsclient.objects.Contain_;
import com.initmed.termsservice.termsclient.objects.Expansion;
import com.initmed.termsservice.termsclient.objects.Parameter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author sd199
 */
@Stateless
public class TermsSync {

    public static final String EGISZ_TARGETS = "1.2.643.2.69.1.1.1.64";

    public static final String EGISZ_RESEARCHTYPES = "1.2.643.2.69.1.1.1.31";

    public static final String EGISZ_WORK_POSITIONS = "1.2.643.5.1.13.2.1.1.607";

    public static final String EGISZ_RESEARCHTYPE = "1.2.643.2.69.1.1.1.57";

    public static final String EGISZ_CASECLASS = "1.2.643.2.69.1.1.1.44";

    public static final String EGISZ_SPECIALITIES = "1.2.643.5.1.13.13.11.1066";

    public static final String EGISZ_ORDERSTATUS = "1.2.643.2.69.1.1.1.45";

    public static final String EGISZ_MEDSERVICEPROFILE = "1.2.643.2.69.1.1.1.56";

    @EJB
    private EgiszTermsVersionsFacade egiszTermsVersionsFacade;
    @EJB
    private TermsClient termsClient;

    public void syncSimpleTerms() throws ParamUrlNotAddedException {
        List<EgiszTermsVersions> egiszTermsVersionses = egiszTermsVersionsFacade.loadEgiszTermsVersions();
        for (EgiszTermsVersions x : egiszTermsVersionses) {
            if (x.getMistablename() != null && x.getMistablename().length() > 1) {
                syncSimpleTerm(x);
            } else {
                Integer version = termsClient.loadVersion(x.getOid());
                x.setVer(version);
                egiszTermsVersionsFacade.edit(x);
            }
        }
    }

    public void syncSimpleTermByOid(String oid) throws ParamUrlNotAddedException {
        EgiszTermsVersions x = egiszTermsVersionsFacade.loadEgiszTermsVersion(oid);
        syncSimpleTerm(x);
    }

    private void syncSimpleTerm(EgiszTermsVersions x) throws ParamUrlNotAddedException {
        Integer version = termsClient.loadVersion(x.getOid());
        if (x.getVer() != version) {
            try {
                Expansion ex = termsClient.loadTerm(x.getOid());
                ex.getContains().forEach((contain) -> {
                    egiszTermsVersionsFacade.updateTwoFileds(x.getMistablename(), contain.getCode(), contain.getDisplay());
                });
                x.setVer(version);
                egiszTermsVersionsFacade.edit(x);

            } catch (TermNotFoundException ex1) {
                Logger.getLogger(TermsSync.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void updateEgiszTargets() throws ParamUrlNotAddedException, TermNotFoundException {
        EgiszTermsVersions egiszTermsVersions = egiszTermsVersionsFacade.loadEgiszTermsVersion(EGISZ_TARGETS);
        Integer version = termsClient.loadVersion(egiszTermsVersions.getOid());
        if (egiszTermsVersions.getVer() != version) {
            Expansion ex = termsClient.loadTerm(egiszTermsVersions.getOid());
            System.out.println(ex.getContains().size());

            ex.getContains().forEach((contain) -> {
                try {
                    String code = null;
                    String name = null;
                    String oid = null;
                    for (Parameter x : termsClient.loadExpandValue(EGISZ_TARGETS, contain.getCode())) {
                        if (x.getName().equals("id")) {
                            code = x.getValueString();
                        }
                        if (x.getName().equals("name")) {
                            name = x.getValueString();
                        }
                        if (x.getName().equals("depart_oid")) {
                            oid = x.getValueString();
                        }
                    }
                    egiszTermsVersionsFacade.updateEiszTargets(code, name, oid);
                } catch (TermNotFoundException ex1) {
                    Logger.getLogger(TermsSync.class.getName()).log(Level.SEVERE, null, ex1);
                }
            });
            egiszTermsVersions.setVer(version);
            egiszTermsVersionsFacade.edit(egiszTermsVersions);
        }
    }

    public void updateEgiszResearchtypes() throws ParamUrlNotAddedException, TermNotFoundException {
        EgiszTermsVersions egiszTermsVersions = egiszTermsVersionsFacade.loadEgiszTermsVersion(EGISZ_RESEARCHTYPES);
        Integer version = termsClient.loadVersion(egiszTermsVersions.getOid());
        if (egiszTermsVersions.getVer() != version) {
            Expansion ex = termsClient.loadTerm(egiszTermsVersions.getOid());
            ex.getContains().forEach((contain) -> {
                egiszTermsVersionsFacade.updateTwoFieldTable(egiszTermsVersions.getMistablename(), "CODE", "NAME", contain.getCode(), contain.getDisplay());
            });
            egiszTermsVersions.setVer(version);
            egiszTermsVersionsFacade.edit(egiszTermsVersions);
        }
    }

    public void updateEgiszResearchType() throws ParamUrlNotAddedException, TermNotFoundException {
        EgiszTermsVersions egiszTermsVersions = egiszTermsVersionsFacade.loadEgiszTermsVersion(EGISZ_RESEARCHTYPE);
        Integer version = termsClient.loadVersion(egiszTermsVersions.getOid());
        if (egiszTermsVersions.getVer() != version) {
            Expansion ex = termsClient.loadTerm(egiszTermsVersions.getOid());
            ex.getContains().forEach((contain) -> {
                egiszTermsVersionsFacade.updateThreeFieldTable(egiszTermsVersions.getMistablename(), "ID", "NAME", "GROUPNAME", contain.getCode(), contain.getDisplay(), contain.getContains().get(0).getDisplay());
            });
            egiszTermsVersions.setVer(version);
            egiszTermsVersionsFacade.edit(egiszTermsVersions);
        }
    }

    public void updateEgiszCaseClass() throws ParamUrlNotAddedException, TermNotFoundException {
        EgiszTermsVersions egiszTermsVersions = egiszTermsVersionsFacade.loadEgiszTermsVersion(EGISZ_CASECLASS);
        Integer version = termsClient.loadVersion(egiszTermsVersions.getOid());
        if (egiszTermsVersions.getVer() != version) {
            Expansion ex = termsClient.loadTerm(egiszTermsVersions.getOid());
            ex.getContains().forEach((contain) -> {
                egiszTermsVersionsFacade.updateTwoFieldTable(egiszTermsVersions.getMistablename(), "CODE", "NAME", contain.getCode().toUpperCase(), contain.getContains().get(1).getDisplay());
            });
            egiszTermsVersions.setVer(version);
            egiszTermsVersionsFacade.edit(egiszTermsVersions);
        }
    }

    public void updateEgiszOrderStatus() throws ParamUrlNotAddedException, TermNotFoundException {
        EgiszTermsVersions egiszTermsVersions = egiszTermsVersionsFacade.loadEgiszTermsVersion(EGISZ_ORDERSTATUS);
        Integer version = termsClient.loadVersion(egiszTermsVersions.getOid());
        if (egiszTermsVersions.getVer() != version) {
            Expansion ex = termsClient.loadTerm(egiszTermsVersions.getOid());
            ex.getContains().forEach((contain) -> {
                egiszTermsVersionsFacade.updateTwoFieldTable(egiszTermsVersions.getMistablename(), "CODE", "NAME", contain.getCode().toUpperCase(), contain.getContains().get(1).getDisplay());
            });
            egiszTermsVersions.setVer(version);
            egiszTermsVersionsFacade.edit(egiszTermsVersions);
        }
    }

    public void updateEgiszMedServiceProfile() throws ParamUrlNotAddedException, TermNotFoundException {
        EgiszTermsVersions egiszTermsVersions = egiszTermsVersionsFacade.loadEgiszTermsVersion(EGISZ_MEDSERVICEPROFILE);
        Integer version = termsClient.loadVersion(egiszTermsVersions.getOid());
        if (egiszTermsVersions.getVer() != version) {
            Expansion ex = termsClient.loadTerm(egiszTermsVersions.getOid());
            ex.getContains().forEach((contain) -> {
                egiszTermsVersionsFacade.updateThreeFieldTable(egiszTermsVersions.getMistablename(), "ID", "NAME", "GROUPNAME", contain.getCode(), contain.getDisplay(), contain.getContains().get(0).getDisplay());
            });
            egiszTermsVersions.setVer(version);
            egiszTermsVersionsFacade.edit(egiszTermsVersions);
        }
    }

}
