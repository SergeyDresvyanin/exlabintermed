/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.termsservice.db.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class EgiszTermsVersionsFacade extends AbstractFacade<EgiszTermsVersions> {

    @PersistenceContext(unitName = "com.initmed_TermsServiceN3_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EgiszTermsVersionsFacade() {
        super(EgiszTermsVersions.class);
    }

    public List<EgiszTermsVersions> loadEgiszTermsVersions() {
        Query q = em.createNativeQuery("SELECT * FROM EGISZ_TERMS_VERSIONS WHERE ISSIMPLE=1", EgiszTermsVersions.class);
        return q.getResultList();
    }

    public EgiszTermsVersions loadEgiszTermsVersion(String oid) {
        Query q = em.createNativeQuery("SELECT * FROM EGISZ_TERMS_VERSIONS WHERE OID=?1", EgiszTermsVersions.class);
        q.setParameter(1, oid);
        return (EgiszTermsVersions) q.getSingleResult();

    }

    public void updateTwoFileds(String tableName, String id, String name) {
        Query q = em.createNativeQuery("IF EXISTS(select * from " + tableName + " where id=?)\n"
                + "   update " + tableName + "  set name=? where id=?\n"
                + "ELSE\n"
                + "   insert into " + tableName + "(ID, NAME, ISVISIBLE) values(?, ?, 1)");
        q.setParameter(1, id);
        q.setParameter(2, name);
        q.setParameter(3, id);
        q.setParameter(4, id);
        q.setParameter(5, name);
        q.executeUpdate();
    }

    public void updateEiszTargets(String code, String name, String oid) {
        Query q = em.createNativeQuery("IF EXISTS(select * from EGISZ_EXLAB_TARGETS where CODE=?)\n"
                + "   update EGISZ_EXLAB_TARGETS set NAME=?, OID=? where CODE=?\n"
                + "ELSE\n"
                + "   insert into EGISZ_EXLAB_TARGETS(CODE, NAME, OID, ISVISIBLE) values(?, ?, ?, 1)");
        q.setParameter(1, code);
        q.setParameter(2, name);
        q.setParameter(3, oid);
        q.setParameter(4, code);
        q.setParameter(5, code);
        q.setParameter(6, name);
        q.setParameter(7, oid);
        q.executeUpdate();
    }

    public void updateTwoFieldTable(String tableName, String columnFirst, String columnSecond, String valueFirst, String valueSecond) {
        Query q = em.createNativeQuery("IF EXISTS(select * from " + tableName + " where " + columnFirst + "=?)\n"
                + "   update " + tableName + "  set " + columnSecond + "=? where " + columnFirst + "=?\n"
                + "ELSE\n"
                + "   insert into " + tableName + "(" + columnFirst + ", " + columnSecond + ", ISVISIBLE) values(?, ?, 1)");
        q.setParameter(1, valueFirst);
        q.setParameter(2, valueSecond);
        q.setParameter(3, valueFirst);
        q.setParameter(4, valueFirst);
        q.setParameter(5, valueSecond);
        q.executeUpdate();
    }

    public void updateThreeFieldTable(String tableName, String columnFirst, String columnSecond, String columnThird, String valueFirst, String valueSecond, String valueThird) {
        Query q = em.createNativeQuery("IF EXISTS(select * from " + tableName + " where " + columnFirst + "=?)\n"
                + "   update " + tableName + " set " + columnSecond + "=?, " + columnThird + "=? where " + columnFirst + "=?\n"
                + "ELSE\n"
                + "   insert into " + tableName + "(" + columnFirst + ", " + columnSecond + ", " + columnThird + "ISVISIBLE) values(?, ?, ?, 1)");
        q.setParameter(1, valueFirst);
        q.setParameter(2, valueSecond);
        q.setParameter(3, valueThird);
        q.setParameter(4, valueFirst);
        q.setParameter(5, valueFirst);
        q.setParameter(6, valueSecond);
        q.setParameter(7, valueThird);
        q.executeUpdate();
    }

}
