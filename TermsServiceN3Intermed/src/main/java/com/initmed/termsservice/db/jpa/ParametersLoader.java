/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.termsservice.db.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class ParametersLoader extends AbstractFacade<Object> {

    @PersistenceContext(unitName = "com.initmed_TermsServiceN3_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ParametersLoader() {
        super(Object.class);
    }

    public String loadParameterByName(String name) {
        Query q = em.createNativeQuery("SELECT PARAMETERVALUE FROM EXLAB_PARAMETERS\n"
                + "WHERE NAME=?1");
        q.setParameter(1, name);
        List<String> res = q.getResultList();
        if (res.isEmpty()) {
            return null;
        } else {
            return res.get(0);
        }

    }
}
