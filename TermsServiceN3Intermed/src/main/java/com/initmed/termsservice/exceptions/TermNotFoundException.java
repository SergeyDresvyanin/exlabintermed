/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.termsservice.exceptions;

/**
 *
 * @author sd199
 */
public class TermNotFoundException extends Exception {

    /**
     * Creates a new instance of <code>TermsNotFound</code> without detail
     * message.
     */
    public TermNotFoundException() {
    }

    /**
     * Constructs an instance of <code>TermsNotFound</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public TermNotFoundException(String msg) {
        super(msg);
    }
}
