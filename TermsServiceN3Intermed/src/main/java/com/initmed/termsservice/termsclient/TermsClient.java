/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.termsservice.termsclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.initmed.termsservice.db.jpa.ParametersLoader;
import com.initmed.termsservice.exceptions.ParamUrlNotAddedException;
import com.initmed.termsservice.exceptions.TermNotFoundException;
import com.initmed.termsservice.termsclient.objects.Expansion;
import com.initmed.termsservice.termsclient.objects.LookUpResponse;
import com.initmed.termsservice.termsclient.objects.Parameter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sd199
 */
@Stateless
public class TermsClient {

    private Client client;

    @EJB
    private ParametersLoader parametersLoader;

    @PostConstruct
    public void init() {
        this.client = ClientBuilder.newClient();
    }

    public Integer loadVersion(String oid) throws ParamUrlNotAddedException {
        String n3Addr = parametersLoader.loadParameterByName("TERMS_URL");
        if (n3Addr == null) {
            throw new ParamUrlNotAddedException();
        }
        WebTarget webTarget = client.target(n3Addr + "/ValueSet/" + oid + "/$versions").queryParam("_format", "JSON");
        Response response = webTarget.request().get();
        if (response != null) {
            try {
                String res = response.readEntity(String.class);
                JSONArray parameter = (new JSONObject(res)).getJSONArray("parameter");
                if (parameter != null && parameter.length() > 0) {
                    JSONObject value = parameter.getJSONObject(0);
                    String versions = value.getString("valueString");
                    if (versions != null) {
                        String resString = versions.split(" ")[0];
                        return Integer.parseInt(resString);
                    }
                }
            } catch (JSONException ex) {
                Logger.getLogger(TermsClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public Expansion loadTerm(String oid) throws TermNotFoundException {
        try {
            String n3Addr = parametersLoader.loadParameterByName("TERMS_URL");
            //parameters
            JSONObject requestObj = new JSONObject();
            requestObj.put("resourceType", "Parameters");

            JSONArray jSONArray = new JSONArray();
            requestObj.put("parameter", jSONArray);

            JSONObject parameterObj = new JSONObject();
            parameterObj.put("name", "system");
            parameterObj.put("valueString", "urn:oid:" + oid);
            jSONArray.put(parameterObj);
            //request
            WebTarget webTarget = client.target(n3Addr + "/ValueSet/$expand").queryParam("_format", "JSON");
            Invocation.Builder invocationBuilder = webTarget.request();
            Response response = invocationBuilder.post(Entity.json(requestObj.toString()));
            if (response.getStatus() == 200) {
                String resString = new JSONObject(response.readEntity(String.class)).getJSONArray("parameter").getJSONObject(0).getJSONObject("resource").getString("expansion");
                System.out.println(resString);
                ObjectMapper objectMapper = new ObjectMapper();
                Expansion exp = objectMapper.readValue(resString, Expansion.class);
                return exp;
            }
        } catch (JSONException ex) {
            Logger.getLogger(TermsClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TermsClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new TermNotFoundException(oid);
    }

    public List<Parameter> loadExpandValue(String termOid, String valueString) throws TermNotFoundException {
        try {
            String n3Addr = parametersLoader.loadParameterByName("TERMS_URL");
            //parameters
            JSONObject requestObj = new JSONObject();
            requestObj.put("resourceType", "Parameters");

            JSONArray jSONArray = new JSONArray();
            requestObj.put("parameter", jSONArray);

            JSONObject parameterObj = new JSONObject();
            parameterObj.put("name", "system");
            parameterObj.put("valueString", "urn:oid:" + termOid);
            jSONArray.put(parameterObj);

            JSONObject valueObj = new JSONObject();
            valueObj.put("name", "code");
            valueObj.put("valueString", valueString);
            jSONArray.put(valueObj);

            //request
            WebTarget webTarget = client.target(n3Addr + "/ValueSet/$lookup").queryParam("_format", "JSON");
            Invocation.Builder invocationBuilder = webTarget.request();

            Response response = invocationBuilder.post(Entity.json(requestObj.toString()));
            if (response.getStatus() == 200) {
                ObjectMapper objectMapper = new ObjectMapper();
                LookUpResponse exp = objectMapper.readValue(response.readEntity(String.class), LookUpResponse.class);
                return exp.getParameter();
            }
        } catch (JSONException ex) {
            Logger.getLogger(TermsClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TermsClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new TermNotFoundException();
    }

}
