package com.initmed.termsservice.termsclient.objects;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Contain implements Serializable {

    private String code;
    private String display;
    private String version;
    private List<Contain_> contains = null;
    private final static long serialVersionUID = -4061370475065039777L;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Contain_> getContains() {
        return contains;
    }

    public void setContains(List<Contain_> contains) {
        this.contains = contains;
    }

}
