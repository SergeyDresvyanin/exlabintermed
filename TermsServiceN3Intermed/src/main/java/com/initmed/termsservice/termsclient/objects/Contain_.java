
package com.initmed.termsservice.termsclient.objects;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Contain_ implements Serializable
{

    private String code;
    private String display;    
    private final static long serialVersionUID = 5506595856749196217L;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }   

}
