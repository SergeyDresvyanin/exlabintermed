
package com.initmed.termsservice.termsclient.objects;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Expansion implements Serializable
{

    private List<Contain> contains = null;
    private List<Parameter_> parameter = null;
    private String timestamp;    
    private final static long serialVersionUID = 7952500102369606529L;

    public List<Contain> getContains() {
        return contains;
    }

    public void setContains(List<Contain> contains) {
        this.contains = contains;
    }

    public List<Parameter_> getParameter() {
        return parameter;
    }

    public void setParameter(List<Parameter_> parameter) {
        this.parameter = parameter;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


}
