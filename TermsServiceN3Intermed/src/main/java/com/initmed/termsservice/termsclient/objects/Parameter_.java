package com.initmed.termsservice.termsclient.objects;

import java.io.Serializable;

public class Parameter_ implements Serializable {

    private String name;
    private String valueString;
    private final static long serialVersionUID = -7987611169566967621L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

}
