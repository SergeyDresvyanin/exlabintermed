/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    updateData();
});
function updateData() {
    $("#info").empty();
    $("#versions").empty();
    $("#contr").empty();

    $.ajax({
        url: 'api/getallterms',
        type: 'get',
        async: true,
        beforeSend: function () {
            $("#ajaxSpinnerContainer").show();
            $("#content").hide();
        },
        success: function (data) {
            var d = JSON.parse(data);
            $("#ajaxSpinnerContainer").hide();
            $("#content").show();
            for (var i = 0; i < d.length; i++) {
                var o = d[i];
                $("#info").append('<li class="list-group-item d-flex justify-content-between align-items-center">OID ' + o.oid + ' Описание ' + o.name + '</li>');
                $("#info").append('<li class="list-group-item d-flex justify-content-between align-items-center">Версия ' + o.ver + '</li>');
            }
        }
    });
}

function updateTerm(oid) {
    $.ajax({
        url: 'api/updateterm/' + oid,
        type: 'get',
        async: true,
        beforeSend: function () {
            $("#ajaxSpinnerContainer").show();
            $("#content").hide();

        },
        success: function (data) {
            updateData();
            $("#ajaxSpinnerContainer").hide();
            $("#content").show();
        },
        error: function (jqXHR, exception) {
        },
    });
}

function updateAll() {
    $.ajax({
        url: 'api/updateall',
        type: 'get',
        async: true,
        beforeSend: function () {
            $("#ajaxSpinnerContainer").show();
            $("#content").hide();
        },
        success: function (data) {
            updateData();
            $("#ajaxSpinnerContainer").hide();
            $("#content").show();
        },
        error: function (jqXHR, exception) {
        },
    });
}

